package it.venis.iris2.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.oggettidbiris.VfixFotografie;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixLogMimuvExample;
import it.venis.iris2.oggettidbiris.VfixLogMimuvMapper;
import it.venis.iris2.oggettidbiris.VfixRefEsterno;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazione;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;
import it.venis.iris2.oggettidbiris.dao.CustomMapper;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import it.venis.iris2.db.utils.CDbUtils;


public class CCoopApplicativa {

	Logger logger = Logger.getLogger("MyLog");  
	FileHandler fh;
	private static final byte INS_SF = 0;
	private static final byte UPD_SF = 1;
	
	
	private CDbUtils dbu = new CDbUtils();

	private AnnotationConfigApplicationContext ctx;
	
	java.util.ResourceBundle mybundle = java.util.ResourceBundle.getBundle("application");

	public CCoopApplicativa() {
		
		 try {
			 
			fh = new FileHandler(mybundle.getString("logging.coop.file"));
		     logger.addHandler(fh);
		     SimpleFormatter formatter = new SimpleFormatter();  
		     fh.setFormatter(formatter); 

		} catch (SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		
		// TODO Auto-generated constructor stub
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();	
	}
	
	protected void finalize() throws Throwable {
        super.finalize();
        logger.removeHandler(fh);
        fh.close();
        ctx.close();
	 }
	
	
	public boolean bDoCooperazione(VfixSegnalazione newSegn, VfixUtenteRegistrato utente, VfixSitoSegnalazione newSS, List<VfixFotografie> foto, byte nTipoOp, boolean bExternal)
	{
		
		Thread t = new Thread(new Runnable() {
	        public void run()
	        {
	    		bElabora (newSegn, utente, newSS, foto, nTipoOp, bExternal);
	        }
		}); 
		
		t.setName("thread " +  newSegn.getIdSegnalazione());
		t.start();
		
		return true;
	}

	private boolean bElabora(VfixSegnalazione newSegn, VfixUtenteRegistrato utente, VfixSitoSegnalazione newSS, List<VfixFotografie> foto, byte nTipoOp, boolean bExternal)
	{
		String sUrlNotifica = "";
		
		
		if (mybundle.getString("cooperazioneapplicativa").equals("1"))
		{
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			//List<Map<String,String>> infoSegn = cm.getUltimoAssegnatario(IdSegn).get(0);
			Short nRefAttuale = cm.getUltimoAssegnatario(newSegn.getIdSegnalazione());
			String sListaRefEsterni = dbu.leggiRefEsterni((long)nRefAttuale);
			
			if (sListaRefEsterni == null) return false;
			
			String [] arrRefEsterni = sListaRefEsterni.split("\\|"); 
			
			for (short idx = 0; idx < arrRefEsterni.length; idx++)
			{
				VfixRefEsterno ref = dbu.leggiRefEsterniUrl(arrRefEsterni[idx]);
				if (ref != null && ref.getAttivo().equals("S"))
				{
					if (!ref.getUrl().equals(""))
						bNotifica_per_Esterno(newSegn, utente, newSS, foto, nTipoOp, ref.getUrl(), ref.getuEsterno(), ref.getpEsterno(), arrRefEsterni[idx], bExternal);
				}
			}
		}
			
		return true;
	}

	// fa il marshall...
	private String sXmlDati (Object dati, @SuppressWarnings("rawtypes") Class classe)
	{
		
		Marshaller marshaller;
		JAXBContext jaxb = null;
		StringWriter stringWriter = new StringWriter();
		try {
			jaxb = JAXBContext.newInstance(classe);
			marshaller = jaxb.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(dati, stringWriter);
			String result = stringWriter.toString();
			System.out.println(result);
			return result;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public boolean bNotifica_per_Esterno(VfixSegnalazione newSegn, VfixUtenteRegistrato utente, VfixSitoSegnalazione newSS, List<VfixFotografie> foto,
		    Byte bTipoOperazione, String sUrlNotifica, String sUsrNotifica, String sPwdNotifica,String sRefEsterno, boolean bExternal)
	{
		String sDati = "";
		// TODO - completare con la serializzazione e l'invio ad Insula - in fase preliminare si possono anche recuperare con IrisBonifiche2
		//Dim t As String = sSerializzaOggettoUTF8(datiNotifica)
		// intercetta il valore di ritorno
		Long idEsterno = 0L; boolean bOpOk = true;

		switch (sRefEsterno)
		{
			case "XXX":
				break;
			case "YYYY":
				break;
			case "ZZZ":
				break;				
			default:
				return false;
		}
		
	
		// salvare su log mimuv!!!!!!!!!!
		if (bOpOk) dbu.setLogEsterno (newSegn.getIdSegnalazione(), idEsterno, sRefEsterno, (short) bTipoOperazione, "S");
		return true; 
	}

	
	public boolean bAggiorna_per_Esterno(Long idSegn, String sRefEsterno)
	{
		String sDati = "";
		try {
			switch (sRefEsterno)
			{
				case "XXXX":
					break;				
				default:
					return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true; 
	}


	
	private String sChiaveRitorno (String sResult, String sRefEsterno)
	{
		Unmarshaller unmarshaller;
		JAXBContext jaxb = null;
		Object obj = null;
		switch (sRefEsterno)
		{
			case "XXX":
				break;
			default:
				return null;
		}		
		return null;
	}
	
	public String sCoopAggiorna (String datiCoopEsterno)
	{
		return null;
	}
	
	
	// verifica se la segnalazione è già stata notificata a SalesForce
	// se aggiorno, deve esserci
	// se cerco di esportarla (per inserimento o per trasferimento), non deve esserci
	public boolean bCheckIsIn_SF (Long idSegn)
	{
		return true;
	}


}
