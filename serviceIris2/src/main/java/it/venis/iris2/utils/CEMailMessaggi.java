package it.venis.iris2.utils;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.SmtpMailSender;
import it.venis.iris2.irisinterface.DatiCooperatore;
import it.venis.iris2.oggettidbiris.VfixCommenti;
import it.venis.iris2.oggettidbiris.VfixInopportune;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;
import it.venis.iris2.oggettidbiris.dao.CustomMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;

import org.apache.commons.codec.Charsets;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;

import com.google.common.io.Files;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CEMailMessaggi {

	public Short COMM_AGGIUNTO = 1;
	public Short COMM_RIMOSSO = 2;
	private JsonParser parser = new JsonParser();
	private JsonObject oDati = null;
	private java.util.ResourceBundle mybundle;
	private AnnotationConfigApplicationContext ctx;
	private Properties props; 


	public CEMailMessaggi() {
		// TODO Auto-generated constructor stub
		props = new Properties();
		try {
			FileInputStream input = new FileInputStream(new ClassPathResource(".\\messaggi.properties").getFile());
			props.load(new InputStreamReader(input, Charset.forName("ISO-8859-1")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
 	   	mybundle=java.util.ResourceBundle.getBundle("application");
 	   	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		
	}
	
	protected void finalize() throws Throwable {
	     try {
	    	 ctx.close();
	     } finally {
	         super.finalize();
	     }
	 }

	
	// compone la e-mail per l'utente...
	public String sStrMessaggio (VfixSegnalazione newSegn, VfixUtenteRegistrato utente, boolean bWithAdmin)
	{
		String szMsg = null;
		String strUrlBO  = null;
		String strUrl = null;
		String strMail = null;
		
		strUrlBO = "<a href=http://irisadmin.comune.venezia.it/frmVediSegnalazioneNew.aspx?id=" + newSegn.getIdSegnalazione() + (bWithAdmin?"&adm=1":"") + ">Vedi segnalazione</a>";		
		strUrl = "<a href=https://iris.comune.venezia.it/dettaglio/" + newSegn.getIdSegnalazione() + ">Vedi segnalazione</a>";
		
	    strMail = (utente.getEmail().equals("") || utente.getEmail().equals("-"))? "-":"<a href='mailto:" + utente.getEmail() +  "'" + ">" + utente.getEmail() + "</a>";
		
		if (bWithAdmin)
		{
	        szMsg = props.getProperty("strMessaggioEMail_1") + " " + newSegn.getIdSegnalazione() + "<br />";
	        szMsg += props.getProperty("strMessaggioEMail_2") + " " + utente.getCognome() + " " + utente.getNome() + "<br />";
            szMsg += props.getProperty("strMessaggioEMail_4") + " " + strMail + "<br />";
            szMsg += props.getProperty("strMessaggioEMail_5") + " " + utente.getTelefono() + "<br />";
            szMsg += props.getProperty("strMessaggioEMail_7") + " " + strUrlBO;
		}
		else
		{
	        szMsg = props.getProperty("strMessaggioEMail_Utente");
	        szMsg = szMsg.replace("%1", newSegn.getIdSegnalazione().toString());
	        szMsg += props.getProperty("strMessaggioEMail_7") + " " + strUrl;
		}
				
		szMsg += props.getProperty("strNoReply");
		return szMsg; 
	}

	
	
	public boolean bEMailOperatoriInopportuna (VfixInopportune dInopportuna)
	{

		String sListaDestin  = CUtils.REF_DEFAULT_DARKROOM.toString();
		
		try {
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			List<Map<String,String>> infoSegn = cm.getInfoSegnalazione(dInopportuna.getPtrSegnalazione());
			String sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(infoSegn.get(0));
			oDati = parser.parse(sJson).getAsJsonObject();
			if (oDati.get("ULT_ASS").getAsInt() != CUtils.REF_DEFAULT_DARKROOM)
				sListaDestin += ", " + oDati.get("ULT_ASS").getAsString();
					
			List<Map<String,String>> destinatari  = cm.getEmailDestinatari(sListaDestin, oDati.get("PTR_MUNIC").getAsInt());
			
			if (destinatari.size() >0)
			{
				String messaggio = "";
				messaggio = props.getProperty("strMailSegnDel");
                messaggio = messaggio.replaceAll("%1", new SimpleDateFormat("dd/MM/yyyy").format(dInopportuna.getData()));
                messaggio = messaggio.replaceAll("%2", dInopportuna.getPtrSegnalazione().toString());
                
    			inviaEMail(destinatari, messaggio, "ATTENZIONE: Cancellazione utente della segnalazione nr. " + dInopportuna.getPtrSegnalazione().toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
		return true;
	}
	
	
	public boolean bEMailOperatoriCommento (VfixCommenti dCommento, int tipoAzione)
	{

		String sListaDestin  = CUtils.REF_DEFAULT_DARKROOM.toString();
		String sAzione = "Aggiunto";
		
		try {
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			List<Map<String,String>> infoSegn = cm.getInfoSegnalazione(dCommento.getPtrSegnalazione());
			String sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(infoSegn.get(0));
			oDati = parser.parse(sJson).getAsJsonObject();
			if (oDati.get("ULT_ASS").getAsInt() != CUtils.REF_DEFAULT_DARKROOM)
				sListaDestin += ", " + oDati.get("ULT_ASS").getAsString();
					
			List<Map<String,String>> destinatari  = cm.getEmailDestinatari(sListaDestin, oDati.get("PTR_MUNIC").getAsInt());
			
			if (destinatari.size() >0)
			{
				String messaggio = "";
                if (tipoAzione == COMM_AGGIUNTO)
                	messaggio = props.getProperty("strMailCommentAdd");
                else
                {
                	messaggio = props.getProperty("strMailCommentDel");
                	sAzione = "Oscurato";
                }
                messaggio = messaggio.replaceAll("%1", new SimpleDateFormat("dd/MM/yyyy").format(dCommento.getData()));
                messaggio = messaggio.replaceAll("%2", dCommento.getPtrSegnalazione().toString());
                
    			inviaEMail(destinatari, messaggio, "ATTENZIONE: " + sAzione + " commento alla segnalazione nr. " + dCommento.getPtrSegnalazione().toString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
		return true;
	}
	
	
	public String inviaEMail(List<Map<String,String>> sLista, String sTesto, String sSubject)
	{
		
		boolean bMailTest = mybundle.getString("mail.test").equals("1");
		String sMittente = mybundle.getString("mail.mittente").toString();
    	
    	if (sLista == null) return null;
    	//String sIndirizzoEMailCCN = "";
    	
    	if (bMailTest) return "";
    	
    	String sMessaggio = "";
		
    	JavaMailSender mSender;
    	mSender= (JavaMailSender) ctx.getBean("javaMailSender");
    	SmtpMailSender ml = new SmtpMailSender(mSender);
    	
    	// divide i multipli...
    	String to = "";
    	String cc = ""; 
    	String bcc = "";
    	
    	for (int idxNodi = 0; idxNodi < sLista.size(); idxNodi++)
    	{  
    		String sIndirizzi = sLista.get(idxNodi).get("EMAIL");
    		if (sIndirizzi.endsWith(";"))
    			sIndirizzi = sIndirizzi.substring(0, sIndirizzi.lastIndexOf(";"));
    		
    		String[] arrDestinatari = sIndirizzi.split(";");
    		
    		for (int idx = 0; idx < arrDestinatari.length; idx++)
    		{
    			if (arrDestinatari[idx].startsWith("#")) cc += arrDestinatari[idx].replace("#", "") + ";";
    			else
    				if (arrDestinatari[idx].startsWith("^")) bcc += arrDestinatari[idx].replace("^", "") + ";";
    				else to += arrDestinatari[idx] + ";";
    		}
    	}
    	if (to!= "") to = to.substring(0, to.lastIndexOf(";"));
    	if (cc!= "") cc = cc.substring(0, cc.lastIndexOf(";"));
    	if (bcc!= "") bcc = bcc.substring(0, bcc.lastIndexOf(";"));
    	
		try 
			{
			ml.send((to!="")?to.split(";"):null, 
					sSubject, 
					sTesto, 
					sMittente, 
					null,
					(cc!="")?cc.split(";"):null,
					(bcc!="")?bcc.split(";"):null,
					null,
					false);
			
		} catch (MessagingException e) {
			sMessaggio = e.getMessage();
			e.printStackTrace();
		}
	    	
	   return null;     
	}
	
	// compone la e-mail per l'utente...
	public String sStrMessaggio_Finale (VfixSegnalazione newSegn, VfixUtenteRegistrato utente)
	{
		String szMsg = null;
		String strUrl = null;
		String strMail = null;
		
		strUrl = "<a href=https://iris.comune.venezia.it/dettaglio/" + newSegn.getIdSegnalazione() + ">Vedi segnalazione</a>";
		
	    strMail = (utente.getEmail().equals("") || utente.getEmail().equals("-"))? "-":"<a href='mailto:" + utente.getEmail() +  "'" + ">" + utente.getEmail() + "</a>";
		
	    szMsg = props.getProperty("strRisolto1");
	    szMsg = szMsg.replace("%1", newSegn.getIdSegnalazione().toString());
	    szMsg += props.getProperty("strMessaggioEMail_7") + " " + strUrl;
				
		szMsg += props.getProperty("strNoReply");
		
		return szMsg; 
	}
	
	// per inviare email di conclusione all'utente...
	public String sStrMessaggio_Finale (Long lIdSegnalazione, String emailUtente)
	{
		String szMsg = null;
		String strUrl = null;
		String strMail = null;
		
		strUrl = "<a href=https://iris.comune.venezia.it/dettaglio/" + lIdSegnalazione + ">Vedi segnalazione</a>";
		
	    strMail = (emailUtente.equals("") || emailUtente.equals("-"))? "-":"<a href='mailto:" + emailUtente +  "'" + ">" + emailUtente + "</a>";
		
	    szMsg = props.getProperty("strRisolto1");
	    szMsg = szMsg.replace("%1", lIdSegnalazione.toString());
	    szMsg += props.getProperty("strMessaggioEMail_7") + " " + strUrl;
				
		szMsg += props.getProperty("strNoReply");
		
		szMsg = szMsg.replaceAll("%2", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		szMsg = szMsg.replaceAll("%5", lIdSegnalazione.toString());

		inviaEMail(emailUtente,"", szMsg, "Operazione su segn. nr. " + lIdSegnalazione.toString());
		
		return szMsg; 
	}

	

	// --- operazione su segnalazione...
	public boolean bEMailOperatoriRiassegnata (DatiCooperatore datiC, String szReferente)
	{
		String sListaDestin  = CUtils.REF_DEFAULT_DARKROOM.toString();
		
		try {
			CustomMapper cm = ctx.getBean(CustomMapper.class);
			List<Map<String,String>> infoSegn = cm.getInfoSegnalazione(Long.parseLong(datiC.getChiaveIris()));
			String sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(infoSegn.get(0));
			oDati = parser.parse(sJson).getAsJsonObject();
			if (oDati.get("ULT_ASS").getAsInt() != CUtils.REF_DEFAULT_DARKROOM)
				sListaDestin += ", " + oDati.get("ULT_ASS").getAsString();
					
			List<Map<String,String>> destinatari  = cm.getEmailDestinatari(sListaDestin, oDati.get("PTR_MUNIC").getAsInt());
			
			if (destinatari.size() >0)
			{
				String messaggio = "";
				String sUrl = "https://irisadmin.comune.venezia.it/frmVediSegnalazioneNew.aspx?id=" + datiC.getChiaveIris() + "&adm=1";
				messaggio = props.getProperty("strRespinto1");
				
                messaggio = messaggio.replaceAll("%2", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
				messaggio = messaggio.replaceAll("%3", szReferente);
                messaggio = messaggio.replaceAll("%4", datiC.getNote());
                messaggio = messaggio.replaceAll("%5", datiC.getChiaveIris());
                messaggio += "<br/><a href='" + sUrl + "'>Vedi segnalazione</a>";
    			inviaEMail(destinatari, messaggio, "Operazione su segn. nr. " + datiC.getChiaveIris() + " - " + oDati.get("SUBJECT").getAsString());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
		return true;
	}
	
	// invia email
	public String inviaEMail(String sIndirizzoEMail, String sIndirizzoEMailCCN, String sTesto, String sSubject)
	{
		
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		boolean bMailTest = mybundle.getString("mail.test").equals("1");
		String sMittente = mybundle.getString("mail.mittente").toString();
    	
    	if (sIndirizzoEMail == "") return null;
    	
    	sIndirizzoEMailCCN = null;
    	
    	String sMessaggio = "";
    	AnnotationConfigApplicationContext ctx;
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();
		
    	JavaMailSender mSender;
    	mSender= (JavaMailSender) ctx.getBean("javaMailSender");
    	SmtpMailSender ml = new SmtpMailSender(mSender);
    	ctx.close();
    	
    	// divide i multipli...   	
    	// controlla se trattasi di una lista di destinatari (es. GRUPPO_1 o TUTTI)	    	
		try 
			{
			ml.send(sIndirizzoEMail.split(";"), 
					sSubject, 
					sTesto, 
					sMittente, 
					null,
					null,
					(sIndirizzoEMailCCN == null? null: sIndirizzoEMailCCN.split(";")),   // CCN
					null,
					false);
			
		} catch (MessagingException e) {
			sMessaggio = e.getMessage();
			e.printStackTrace();
		}
	    	
	   return null;     
	}
}
