package it.venis.iris2.utils;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.SmtpMailSender;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;
import it.venis.iris2.security.CGestioneUtente;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;

public class CUtils {
	public static final Integer REF_DEFAULT_DARKROOM = 0;
	public static final byte N_OP_INOLTRATA = 10;
	public static final byte N_OP_IN_CARICO = 20;
	public static final byte N_OP_RISOLTA = 30;
	public static final byte N_OP_SOSPESA = 40;
	public static final byte N_OP_TRASFERIMENTO = 50;
	public static final byte N_OP_RESPINTA = 60;
	public static final byte N_OP_CHIUSA = 70;

	private String sPathFotografie = null;

	public CUtils() {
		// TODO Auto-generated constructor stub
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		sPathFotografie = mybundle.getString("path.fotografie");
	}
	
    // composizione del nome file della fotografia...
    public String sComponiNomeFileImmagine (String sNomeFile)
    {
		return (new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()) + "_" + sNomeFile).replace("'", ""); 
    }

	
	public boolean bCopiaRemota(String sNomeFile, String sContenuto) {	
		//sContenuto = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAB+AH4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDy+5t4oMor5dfvZ7iq8e4nKADHPNE7iRyxbORxxUbArjkcjPBoAewGxnZwSCAB603/AGs9KYckZ5pBk9MmgCSV/MOSAD2wKdHHkFWUqwGVyMbqS3kEcwfaHdfuKfX1qZppjOryli65O5s0AVzExlKAfNnpUwKQoVCbmzyx7H2psYAI4y2e9S5UqScdqAIGUsQcckZ4FMyauZ2AMV27vUdRnpVSRhuHTJ5IoAXdhcHv6U3fnGPpQEeQ4SN2/wB1SatRadM20zFbZGOA04ZQfpxSuhqLexEWOVwMYA/GkmCB8pgjuKsQ2cs2QhjCnu0gFWRoUu0F7yyjB7vNQ5JFRpzlsjHYknPFSxnIxTSuCcHODgEd6BknJPJpkA554p9mQbyBWA2mRQw9QTyKjI5rQ0XTY9RupFm8wRRpuJTg5JAH+falJ2V2XTi5SSR2x02whTy106FlJxgrn+efzqo2nWlvrGnTWcKwyMXBVVOGHAPHY4J/KkttOQyxwrcaqpY7Rm5wB+lGg3F2thJPcT3MrpcNEmH7Djn1rkWmtz15WlaKVv8AgF5IJbdwy20GQTtfYAevt7d6q628z6HdJMuEIUhmxkZce/p/Krv24EKTHMc8kb84qlqkq3Gl3oMXCwGQbjn5lwVP60le6KnbkaT6DVs9Jzm1tNPlVehLg7v/AB7uKsJLa2V2FhWyjdrckqhULuDj364NV7e3WIQzvJcM+xGbakZGWXJ425A6/lUWsWlveXmmO8BUNciGRcAZByecdxiq3dmQ7xjzRWprPcG6iKy/Y5SuSN5Rvpjmql7bi4spY5ILdI/KfIjCjnBxz7ECq6aTpC/e0kE45+Zjn6c+3SmXFjpw0m7MNhFDIEALoOxcDjPtS06FPms72+//AIBX0/WrOHTraNdRMMiRIpXYxAIBz2x1xVqTWbCaS3DagkpWQ4L5G3KMMnI9x+VXB4f0yKQ7dOjYjOAxJH6mqMui2EmstAsDQ7bUSlIDgk7iOh9qacG7kONeMUtPxLkd+hHy3tmflHSRcn/69Y/iG4judOjgNzDJNHKpVUcMcbcHpV19D0dgRi7cA4z5g59KWPSNHAjRLVm3NjdI5zj86a5U7kzVWceXTXzOIbrgdBSDrTyMOwxjnHPam4+auo8knkVFYlcggcqRRbXc9mkyQsyNNtG9WKlcHPaldArYGcY5zUW5FuUZv9WGBbPp3pPUcW07o6mPTdYBZJfEWzBKkBnP68VWh0O7guxD/ariN43l3QFs5XGcjI5OatpqGsz7X/4R9X53BmJHXHPP0FQzXWsf2raRpYRW08kTIsbNuDKSM5OePu1zrm20/A9KSp2vZ/iX49Mkt3Yt4gvWCgkovU49M5qtqVnfuIra21eaZbnerLMQFAUZOSK0buDXftLhLHT2RsHLSHP86Y+na2zRXTwacqQFsr5jYO4YOfypJu+/5FtJxsov8TLg0rWliKjWljVcDaJXIHp24qxBoVxc2y3F1rFxvSRgoALbSrbcjmrn2rVzJNDFa2EbRNtctI2CcZ7/AOeKgjTxBBGyqdOKbnk2s2eSSxx+tO8vInkh2b+8e2m3kRULr8wA6fKSTnp3qvd6bc3EKQHWrmVZZVT94vyngnPHPaprefXblEmWHTsOqt82QcMOM4p88OsykEf2bHJCwl+Qsc8HH9aV3fdF8sXHRP8AH/Moro97LtDa2wGAVUGQnmn6Tp0ljq0sr3+9olTLbSVZXzjOTn3pbG51G+tEmhW1AI2nbbO2Mds57cdKfC+pWl5NJdW0Hl3DRxieXMaDAO35QSeelO71TM1GF4ySf4/5m6sijGbhD0BABJ5OM9aw9c1q402/hjheOeF4xJkrtzkn07VomDUA20LpakHON8nFUNS8Py6hIk019bRiOPbiKM4ABJPVveohyp6m9b2jh7m5yV1O11dzXDKqtK5YgdBmoBVnUbN9PvHtndXK4O5ehBGR+lUwTmutWtoePJNSfNuXXcMwLemKijm8m6imC7/LcOEPRsHpSsBggDpV3RHt4dRinu2RYlYqS/RTglT+Yok7IdNXklexsr4js7pi1zZ3UJxwUXcP6VG+vWkOqWdzb21xMLdXyrrtLbvz6frmtb+0bb5mi1iIRhSTmTJ9f8/jVa0vku9Zu2t7oSH7PEFdSecMSR+uOa5lbex6kuZ2XMvuXr3IY9aifIGnX5B7qoJ9fSr39t6dbwywy29+rSYZQYgPyz25q8014sWWmVRkZO7b3z1/Lisy91AW+ow3N0ZHUWrI0kKFlUlwecdOBSTTewSjKK1f4FP+3J/t11Lb2XmQzyB8SNtbgEdvqaiTxKuWSHSzlQSR5xP49Ocf41oRa5pxZX+3XBAP3TE/Nc7ol3Dp9+0l3E6q42rIVOE/CrSTT0MZTlGSSnv6aF6DxJE6xpJYFn2hP3LgDjjgY/Src2uC3u1jTSrn7QDlY2cYPXpgZI5NXToVlcXkV2oBT75Vejnsc+lUZNYs5vEUTXD+RDaEiOQrgs3Q59F60vdeyLaqQXvytrpsGkTSR2sVnc6dequ9v3qxnaoJzyMZ71dvhN5YgtdMupx50cnmMAqnaQcevt0pV1Ox+YjW4Mn/AGzxUn2+zKsDrNuVPGC9S73vY0SXJy835GU/iALcyJLpUwmBO9BJyM9Rjb706LUw5yNGvunVe3/jtS2us2UN9qMhvVjMt0XRgCQy7cA8ds1aXxBp6uWl1KNxjOArA/TpVNdo/mRF33mvwOR1a8W/1F7lI2RGVQFc5PAA/pVNIyxOBmhnDMTkkfw59O1JyvHrzXQlZWPMlJybbLojL8nCqQOTW1oOj6dewzTXCSyMrbFjB46Zzkc1l4MRKNxkBgD3qXTt0t1DYF3FvPOvmKrYzSne2hdFpTV1c2z4dsjOg/s+VMsAGEjYHvyKfpE5SxzbxxM3myIzHG4gNxnp2pq6TpkgUypdlccg3BI6ZPfpxVLUtI01bBXtoGjnaaOPPmFh8xIOMn2rn30bPRs4e9FL+vkdILi7z8lupB79xVLUfOaS1+0sYonn2MjnCMpQ5B9RkfrVP/hHNOgkIMF64UkZEuO+Ow607+zNMiu/s0lq7h7YTqsjl8YYg9T3GKlKPQ0cqjVn+f8AwDTt5XSOOOGODy0RcbGHHA4xntn9DWRs/tTxRNbXrySQwKJEgzlA2BnP5086Bo4ZnNtPkn7iybQP8BQNK0yJhNapcwSKrEOJiCAFPT8RTVlexMueSSdrJ9/+AJqXiT+z9RW3S33RRnbJ2J/3fpVTWprORINWsZk+0Odmx0DBuOcg9CPerGm6PZy6ZbXT6ebmSWESOxdiSxJz7dh+dWbjTbGJbaM2KQq90gZSDhgQeuf5U04p6ESVWcW21Z6ryNFov3YEdjFtwMsqhc8Z6j/IqitpZWmr2yy2MCfaoWIBwwDA5BOe5HFalnYabCSX02B1Ufd28H/PpVHW7KwuLa5uDaLHKQoDrxtBYDgdOB7VMWtjWalvZaf12HmBJ0xJpMDgcqPL/rTUtvKBWPS4oxuHKxZ79enTFVh4ZsoWyrXjBf4RLtJ/IVz3iC1jsZ4Fge5Cyx78SSlu5FOKUnZMipOVOPNKP9fcVdbhWDV7hQMBmLADoMk1QHJpZZ5JhGJDny02Lx2/yaRa6lojyptOTaNq+uLeeSVYw3lqgEOeo9fzpdKt3u9WgWKTy2yzB846D+dU7i4Xyox5Ko4GCV/iFP0ya6i1CF7ONZJ0ViFY8Ywc+naiWzHTtzq50gtNVI2nxGnHU7P61HLor3EJF5r0kwBDBFXI64zjNPibWWB3aLbkY+bFxt46+p+tLNdX8EtuJNLtUFzJtQ/aCQWPriua7vp+h6bULe8n/wCTGbFpN8XnSHVpIo4p2hXcz87e/HTrT5dKvbSGTUItbMk8UTHIDZIxkgE1cij1SOa43S6YqySPLtYlsFuvTtilSHV71buBjYxxgGIssZG7Izke3zU+Z9yFTjbZ3+f+ZJb29zJBE02vXyl0VyAoxyM8HHarAsPN3QvrOourjbgkANkHI6e1Pt7XUIoIo3gtJlRFTJcgkAY/u1ekeK2t57iW1QOkZkAQ5OR+FZtu+h0qCUbv9TltO0xW02CVr++QuMiOF8KOSAOfpT77Tj5Nq8V9esrXKR5nfcFJ6MB6g0+10vWLeNYU1iOBF6R8kLnnHSlvbPVjfWNq+oLdGQmRdy7VBTnn14rW/vbnI4pU/hf9fMu/YrpNwHiCYyD+8gI64/pSG0mmtZIb/WPNRlJKwqAWH4irP2bVQ5Zk0s55yTJzmobg3kUcLC2sJjO6ooSR1zkEjk+wqLv+rHRyq17P8SrY2rFLiG8v74yQTbMRTEDbt3KfyBqs2hQzSef/AGnczSICyGRM8jJAzn2qbydXF5PcRXNhbiXDMhfeBtGO4rOuta1O0vZoJJ4y8eU/dxqFOR1HGatKTejMJyhGPvxf9fMwZMO7SZ5Yk4+tNUc0/YwX7px60gxjr82entXQeaKenzE9OK0dBkjhv5pnV2EdvIwVOp4wcfgTWUSePatDREP9pxzliscBEkpGfug89KmXws0o/wARG9BrVoesN+uB/wA8gQP1/Sm3t/JNFb/Y9PvZPKnWZXkiIU4GMYHrV1NctnYkaovPqrccn/Zq1b3ZvmaCDUFmZl+4Ac46c8euK59tbHpW5lbn/L/MzItU1G6l3jQS6q+JNpbIIHTnoRmpv7SvoFnlutHugnmGTKkYUYAwfyqed1s7yZrrEa3NxI0TlguMIoOc9OgxTrry5rCaO1mWSUptUGZDnkd8+n8qWnYaUkn72q9P8gj1OZhzo+oDtnav+NPvr+ZNLlk/s+5RAFLGQqAoBHoSarPrlh9qkDXNxFiQ7kMZ4OeRwfw4qQ6rp0kEkcl2ZElRlIZCBkjqe/5UKOuwSqrla5vyJrfxBJO4b+xLqTupBwB+la1xqatbJdXtkbYxArAsi5kct1xgegx+NZw1WBxvgN0FUAFdoJHGPXmqWo69aT6lpzGV2jtW3SSMuSM9OhPOVq+W3QzdVPXmv9w1dVV2JbSNQz1yqg/0qC6vrlo4vsOkXgdJVk/fJ8pwpHb61bXX7Vix/tSPc399WwD+VPbVbchv+Jxa89Bvxj9OtTr2/M00a+P8jHi1LUZpWjOmKfLYB8K+VI/HrUOq2M97ctdR2k0LNzMZCNowB0x06VOuuWUN9frLJKyPctJHJGNwC7cDHP41I+u6a0EkaXNzI7RsqgxHkkEDv7/pV6p6IwfJKLUp/kc06qDgybh6jkCmSsjzsYxhOij2qHNOB5rc88DjNSRSyWzl4X2kqVP0Iwf0NEaruVnGVzyB3FMbLEk9TzQCdtUEUjxnKSOn+6xFXIb+7RspczAgf89DVIKamAwMUrIabWxcvdXu9QhVLqQSlPuswywH1qi7eaV3AfKNo4ppzTgu0c0wbb3JYWRdxfJYDCgdKngcecC3bt6VUI6YqzGgNxFt4Ulc0CNSS6Mds0gyHPyr2A/CsVh2HOeatTyGcgdMcCqcnXavAHf1oAZkdqExtdjgkDAzRtywXtnmjALHj5c8UALIAyrIi4UDBqCp8soKA8VEMq+RQA0jCjpk96FPPNWJ4AqllwE3YA78/wD6qYsahQT1NAH/2Q==";
		String sStringBase64 = sContenuto.split(",")[1];
		byte[] valueDecoded= Base64.decodeBase64(sStringBase64);
		//sNomeFile = sComponiNomeFileImmagine (sNomeFile);
		String remotePhotoServer = sPathFotografie + sNomeFile;
		String remoteThumbPhotoServer = sPathFotografie + "THUMB_" + sNomeFile;
		NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("VNS", "Administrator", "v3n1siswebapp");	
		try {
			SmbFile dir = new SmbFile(remotePhotoServer, auth);
			try {
				try (OutputStream out = dir.getOutputStream())
				{
					out.write(valueDecoded);
				}
				
				/*try {
					// creazione del thumbnail
					BufferedImage img = ImageIO.read(new ByteArrayInputStream(valueDecoded));
					int nW=100; int nH=100;
					if (img.getHeight() > img.getWidth())
						nW = (int) ((img.getWidth() * 100) / img.getHeight());
					else
						nH = (int) ((img.getHeight() * 100) / img.getWidth());
					Image scaledImage = img.getScaledInstance(nW, nH, Image.SCALE_FAST);
					
					BufferedImage buffered = new BufferedImage(nW, nH, BufferedImage.TYPE_INT_RGB);
					buffered.getGraphics().drawImage(scaledImage, 0, 0 , null);
					
					WritableRaster raster = buffered.getRaster();
					DataBufferByte buffer = (DataBufferByte) raster.getDataBuffer();
							
					byte[] valueDecodedThumb= buffer.getData();
					
					SmbFile dirThumb = new SmbFile(remoteThumbPhotoServer, auth);
					
					try (OutputStream out2 = dirThumb.getOutputStream())
					{
						out2.write(valueDecodedThumb);
					}
					// fine creazione del thumbnail
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				*/
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	
	    return true;
	}

	// compone la e-mail per l'utente...
	public String sStrMessaggio (VfixSegnalazione newSegn, VfixUtenteRegistrato utente, boolean bWithAdmin)
	{
		String szMsg = null;
		String strUrlBO  = null;
		String strUrl = null;
		String strMail = null;
		
		strUrlBO = "<a href=http://irisadmin.comune.venezia.it/frmVediSegnalazioneNew.aspx?id=" + newSegn.getIdSegnalazione() + (bWithAdmin?"&adm=1":"") + ">Vedi segnalazione</a>";		
		strUrl = "<a href=https://iris.comune.venezia.it/dettaglio/" + newSegn.getIdSegnalazione() + ">Vedi segnalazione</a>";
		
	    strMail = (utente.getEmail().equals("") || utente.getEmail().equals("-"))? "-":"<a href='mailto:" + utente.getEmail() +  "'" + ">" + utente.getEmail() + "</a>";
		
		if (bWithAdmin)
		{
	        szMsg = "E'arrivata una nuova segnalazione IRIS nr." + " " + newSegn.getIdSegnalazione() + "<br />";
	        szMsg += "Segnalatore:" + " " + utente.getCognome() + " " + utente.getNome() + "<br />";
            szMsg += "e-mail:" + " " + strMail + "<br />";
            szMsg += "Telefono 1:" + " " + utente.getTelefono() + "<br />";
            szMsg += "Per visualizzare la segnalazione:" + " " + strUrlBO;
		}
		else
		{
	        szMsg = "Gentile utente, <br/>la tua segnalazione è stata registrata in IRIS con il numero %1.<br/><br/>Potrai seguire le azioni che verranno intraprese per la risoluzione del problema dalla scheda di dettaglio della segnalazione.<br/><br/>Ti ricordiamo che attraverso il sito di IRIS puoi anche monitorare e discutere le segnalazioni degli altri cittadini.<br/>";
	        szMsg = szMsg.replace("%1", newSegn.getIdSegnalazione().toString());
	        szMsg += "Per visualizzare la segnalazione:" + " " + strUrl;
		}
				
		szMsg += "<br /><br />Attenzione: questo messaggio ha il solo scopo di notificare l'avvenuta operazione. Non inviare mail di ritorno al mittente della presente perche' verrebbero automaticamente cestinate.";
		return szMsg; 
	}

	

	public String getEMailUtente(VfixUtenteRegistrato utente)
	{
		String sEMailUtente = utente.getEmail();
		if (sEMailUtente.equals("") || sEMailUtente.equals("-"))
		{
			sEMailUtente = "";
	    	CGestioneUtente gut = new CGestioneUtente();
	    	if (utente.getBearer()!=null)
		    	if (gut.sGetDatiBySpid(utente.getBearer())) sEMailUtente = gut.getsEMail();
		    	else if (gut.sGetDatiByDiMe(utente.getBearer())) sEMailUtente = gut.getsEMail();
		}
		
		return sEMailUtente;
	}
}
