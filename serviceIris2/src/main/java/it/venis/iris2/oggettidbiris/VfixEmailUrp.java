package it.venis.iris2.oggettidbiris;

public class VfixEmailUrp {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_EMAIL_URP.EMAIL
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String email;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_EMAIL_URP.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private String valido;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_EMAIL_URP.EMAIL
	 * @return  the value of VFIX_EMAIL_URP.EMAIL
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_EMAIL_URP.EMAIL
	 * @param email  the value for VFIX_EMAIL_URP.EMAIL
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_EMAIL_URP.VALIDO
	 * @return  the value of VFIX_EMAIL_URP.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public String getValido() {
		return valido;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_EMAIL_URP.VALIDO
	 * @param valido  the value for VFIX_EMAIL_URP.VALIDO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setValido(String valido) {
		this.valido = valido;
	}
}