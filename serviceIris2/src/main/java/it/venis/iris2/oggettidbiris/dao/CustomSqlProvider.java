package it.venis.iris2.oggettidbiris.dao;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.type.JdbcType;

import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.oggettidbiris.VfixAssegnazione;
import it.venis.iris2.oggettidbiris.VfixCommenti;
import it.venis.iris2.oggettidbiris.VfixConcluse;
import it.venis.iris2.oggettidbiris.VfixFotografie;
import it.venis.iris2.oggettidbiris.VfixInopportune;
import it.venis.iris2.oggettidbiris.VfixIterSegnalazione;
import it.venis.iris2.oggettidbiris.VfixLogMimuv;
import it.venis.iris2.oggettidbiris.VfixSegnalazione;
import it.venis.iris2.oggettidbiris.VfixSegnalazione_SIT;
import it.venis.iris2.oggettidbiris.VfixSitoSegnalazione;
import it.venis.iris2.oggettidbiris.VfixUtenteRegistrato;

public class CustomSqlProvider {
	private String sCodiceComune = null;
	private boolean bIrisRoomOn = true;

	public enum TipoExec {
		INSERT_UTENTE
	};	

	public CustomSqlProvider() {
		// TODO Auto-generated constructor stub
		// TODO Auto-generated constructor stub
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		sCodiceComune = mybundle.getString("codicecomune");
		bIrisRoomOn = mybundle.getString("irisroom.on").equals("1");
		
		if (sCodiceComune == null) sCodiceComune = "27042";		
	}

	
	public String selectListaSegnalazioni(Long lIdSegnalazione)
	{
		String s = null;		
/*		s = "SELECT * from (SELECT a.id_segnalazione, to_char(a.data_segnalazione, 'DD/MM/YYYY') Data, a.subject Descrizione, t.data dt_oper, " +
		"c.descrizione Tipologia, b.cognome || ' ' || b.nome Segnalatore, " + 
		"d.ptr_tipo_stato, e.descrizione || PACK_VFIX.sdescrizionesubstato(a.id_segnalazione)  Stato , a.indicazioni, " +
		"decode (g.id_referente, null, PACK_VFIX.DesRefSpecializzatoByRef(u.ptr_refer, ss.ptr_munic), PACK_VFIX.DesRefSpecializzatoByRef(g.id_referente, ss.ptr_munic)) Rif, " + 
		"l.note, l.data_prevista, to_char (round(l.data_prevista - a.data_segnalazione)) ggtotali, to_char (round(l.data_prevista - sysdate)) ggmancanti, (select count(*) " +
		"from vfix_commenti comm where comm.ptr_segnalazione = a.id_segnalazione) nrcommenti, 'S' proprio, ss.latitudine, ss.longitudine " +
		"FROM vfix_segnalazione a, vfix_sito_segnalazione ss, vfix_utente_registrato b, " +
		"vfix_tipo_problema c, vfix_iter_segnalazione d, vfix_tipo_stato e,  vfix_referente g, vfix_tempi l,vfix_utenti u, " +
		"(select ptr_segnalazione , max(data_oper) data from vfix_iter_segnalazione group by ptr_segnalazione) t, (select ptr_segnalazione, max(log_data) dt " +
		"FROM vfix_tempi group by ptr_segnalazione) z  where b.id_utente_registrato = a.ptr_utente and c.id_tipo = a.fk_tipologia  and c.ref_unico is null " +
		"and d.data_oper = t.data and d.ptr_segnalazione = a.id_segnalazione  and t.ptr_segnalazione = a.id_segnalazione and e.id_tipo = d.ptr_tipo_stato and a.valida in ('S', 'V') " +
		"and g.id_referente = PACK_VFIX.getultassegn(a.id_segnalazione) and l.log_data (+)= z.dt and l.ptr_segnalazione (+)= z.ptr_segnalazione " +
		"and z.ptr_segnalazione (+)= a.id_segnalazione  and ss.ptr_segnalazione = a.id_segnalazione  and a.app_name = 'IRIS' and u.id_utente (+)= d.ptr_utente " +
		"order by a.data_segnalazione desc, a.id_segnalazione desc)  where rownum <= 3 ";*/
//		s = "SELECT * from (SELECT a.id_segnalazione, to_char(a.data_segnalazione, 'DD/MM/YYYY') Data, " + 
//		"TO_CHAR(a.data_segnalazione, 'yyyy-MM-dd HH24:MI:SS') data_segnalazione, a.subject oggetto, " +
//		"TO_CHAR(t.data, 'yyyy-MM-dd HH24:MI:SS') dt_oper, " +
//		"a.fk_tipologia, c.descrizione Tipologia, " + 
//		"d.ptr_tipo_stato, e.descrizione || PACK_VFIX.sdescrizionesubstato(a.id_segnalazione, d.ptr_tipo_stato, ':')  Stato , a.indicazioni, " +
//		"ss.ptr_munic, g.id_referente, " +
//		"decode (g.id_referente, null, PACK_VFIX.DesRefSpecializzatoByRef(u.ptr_refer, ss.ptr_munic), PACK_VFIX.DesRefSpecializzatoByRef(g.id_referente, ss.ptr_munic)) Rif, " + 
//		"(select count(*) " +
//		"from vfix_commenti comm where comm.ptr_segnalazione = a.id_segnalazione) nrcommenti, 'S' proprio, ss.latitudine, ss.longitudine, ss.indirizzo_sito " +
//		"FROM vfix_segnalazione a, vfix_sito_segnalazione ss, vfix_utente_registrato b, " +
//		"vfix_tipo_problema c, vfix_iter_segnalazione d, vfix_tipo_stato e,  vfix_referente g, vfix_utenti u, " +
//		"(select ptr_segnalazione , max(data_oper) data from vfix_iter_segnalazione group by ptr_segnalazione) t, (select ptr_segnalazione, max(log_data) dt " +
//		"FROM vfix_tempi group by ptr_segnalazione) z  where b.id_utente_registrato = a.ptr_utente and c.id_tipo = a.fk_tipologia  and c.ref_unico is null " +
//		"and d.data_oper = t.data and d.ptr_segnalazione = a.id_segnalazione  and t.ptr_segnalazione = a.id_segnalazione and e.id_tipo = d.ptr_tipo_stato and a.valida in ('S', 'V') " +
//		"and g.id_referente = PACK_VFIX.getultassegn(a.id_segnalazione) " +
//		"and z.ptr_segnalazione (+)= a.id_segnalazione  and ss.ptr_segnalazione = a.id_segnalazione  and a.app_name = 'IRIS' and u.id_utente (+)= d.ptr_utente ";
//		if (lIdSegnalazione != 0L) s +=	" and id_segnalazione = " + lIdSegnalazione.toString();
		
		
		// occhio che è stato mascherato anche qui lo stato...
		s = "SELECT A.* from (SELECT a.id_segnalazione, " + 
		"TO_CHAR(a.data_segnalazione, 'yyyy-MM-dd HH24:MI:SS') data_segnalazione, a.subject oggetto, a.ptr_utente, " +
		"a.fk_tipologia, c.descrizione Tipologia, decode(d.ptr_tipo_stato, 50, 20, 60, 20, d.ptr_tipo_stato) ptr_tipo_stato, " +
		"decode(e.id_tipo, 50, 'In carico', 60, 'In carico', e.descrizione) || PACK_VFIX.sdescrizionesubstato(a.id_segnalazione, d.ptr_tipo_stato, ':')  Stato , a.indicazioni, " +
		"ss.ptr_munic, m.descrizione municipalita, " +
		"decode (g.id_referente, null, PACK_VFIX.DesRefSpecializzatoByRef(u.ptr_refer, ss.ptr_munic), PACK_VFIX.DesRefSpecializzatoByRef(g.id_referente, ss.ptr_munic)) Rif, " + 
		//"(select count(*) from vfix_commenti comm where comm.ptr_segnalazione = a.id_segnalazione) nrcommenti, " +
		"ss.latitudine, ss.longitudine, ss.indirizzo_sito " +
		"FROM vfix_segnalazione a, vfix_sito_segnalazione ss, vfix_utente_registrato b, " +
		"vfix_tipo_problema c, vfix_iter_segnalazione d, vfix_tipo_stato e,  vfix_referente g, vfix_utenti u, vfix_municipalita m, " +
		"(select ptr_segnalazione , max(data_oper) data from vfix_iter_segnalazione group by ptr_segnalazione) t, (select ptr_segnalazione, max(log_data) dt " +
		"FROM vfix_tempi group by ptr_segnalazione) z  where b.id_utente_registrato = a.ptr_utente and c.id_tipo = a.fk_tipologia  and c.ref_unico is null " +
		"and d.data_oper = t.data and d.ptr_segnalazione = a.id_segnalazione  and t.ptr_segnalazione = a.id_segnalazione and e.id_tipo = d.ptr_tipo_stato and a.valida in ('S', 'V') " +
		"and g.id_referente = PACK_VFIX.getultassegn(a.id_segnalazione) " +
		"and z.ptr_segnalazione (+)= a.id_segnalazione  and ss.ptr_segnalazione = a.id_segnalazione  and a.app_name = 'IRIS' and u.id_utente (+)= d.ptr_utente and m.cod_munic = ss.ptr_munic";
		if (lIdSegnalazione != 0L) s +=	" and id_segnalazione = " + lIdSegnalazione.toString();		
		
		//s += " order by a.data_segnalazione desc, a.id_segnalazione desc)  where rownum <= 2000 ";		
		s += " order by a.data_segnalazione desc, a.id_segnalazione desc) A";
		return s;
	}

	
	public String selectIterSegnalazione(Long lIdSegnalazione)
	{
		// ci sarebbe la casistica masked e non masked (ma come fare per l'utente?)...
		
		String s = null;
		
		s = "SELECT a.ptr_segnalazione, a.ptr_tipo_stato, TO_CHAR(a.data_oper, 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
		"a.note, decode (a.ptr_tipo_stato, 50, 'In carico', 60, 'In carico', b.descrizione)  || PACK_VFIX.sdescrizionesubstato(a.ptr_segnalazione, a.ptr_tipo_stato, ':') descrizione, " +
		"nvl(decode (a.ptr_tipo_stato, 10, PACK_VFIX.listareferentinew(a.ptr_segnalazione, a.ptr_tipo_stato, 'N'), PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione, f.id_referente)), '-') refs, " +
		"decode (a.ptr_tipo_stato, 20, '-', PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,c.id_referente))  des_oper, c.id_referente, nvl(e.ptr_riga, a.id_riga) ptr_riga " +
		"FROM vfix_iter_segnalazione a, vfix_tipo_stato b, vfix_referente c, vfix_utenti d, vfix_assegnazione e, vfix_referente f " +
		"where b.id_tipo = a.ptr_tipo_stato and a.ptr_segnalazione = " + lIdSegnalazione +  " " +
		"and d.id_utente = a.ptr_utente and c.id_referente = d.ptr_refer " +
		"and f.id_referente (+)= e.ptr_ref and e.ptr_riga (+)= a.id_riga " +
		"order by a.data_oper desc";
		
		return s;
	}
	
	public String selectFotoSegnalazione(Long lIdSegnalazione)
	{
		String s = null;
		
		s = "SELECT rownum PictureID, ptr_segnalazione, nome_file_thumb PictureURL, nome_file PictureRealURL, " +
		"nome_file_thumb PictureRealURL_Thumb, interno, ptr_utente " +
		"FROM vfix_fotografie WHERE ptr_segnalazione = " + lIdSegnalazione +  " " +
		"and interno <> 'E' order by interno, PictureID";
		
		return s;
	}

	public String selectListaProblemi(String dimeOn)
	{
		String s = null;
		
		//s = "SELECT id_tipo, descrizione FROM vfix_tipo_problema WHERE app_name = 'IRIS' and attiva = 1 and ref_unico is null order by id_tipo";
		s = "SELECT id_tipo, descrizione FROM vfix_tipo_problema WHERE app_name = 'IRIS' and attiva = 1 and ref_unico is null ";
		
		if (dimeOn.equals("S"))
			s += "and dime_on = 'S' ";
		s += "order by descrizione";
		
		return s;
	}
	
	public String selectListaTipoStato()
	{
		String s = null;
		
		// sono letti mascherati (essendo solo front). Il vero stato non viene indicato nemmeno all'operatore.	
		s = "SELECT id_tipo, descrizione, colore FROM vfix_tipo_stato WHERE app_name = 'IRIS' and masked = 'N' order by id_tipo";
		
		return s;
	}

	
	public String selectListaTipoOperazioni()
	{
		String s = null;
		
		// la Iris Room può essere abilitata o meno...
		
		String sTipoOp = "('OP')"; 
		if (bIrisRoomOn) sTipoOp = "('OP', 'IR')";
		s = "SELECT id_tipo, descrizione, colore FROM vfix_tipo_stato WHERE app_name = 'IRIS' and t_op in " + sTipoOp;
		s += " order by id_tipo";
		return s;
	}
	
	
	
	public String selectListaSubStato()
	{
		String s = null;
		
		s = "SELECT id_subtipo, descrizione FROM vfix_subtipo_chiusura order by id_subtipo";
		
		return s;
	}

	
	public String selectListaMunicipalita()
	{
		String s = null;
		
		s = "SELECT cod_munic, descrizione FROM vfix_municipalita WHERE abilitata = 'S' order by cod_munic";
		
		return s;
	}

	
	public String selectCommentiSegnalazione(Long lIdSegnalazione)
	{
		String s = null;
		
		s = "SELECT c.ptr_segnalazione, c.progr, c.testo, c.firma, TO_CHAR(c.data, 'yyyy-MM-dd HH24:MI:SS') data_commento, c.idea, c.interno,c.titolo FROM vfix_commenti c " +
			"WHERE c.valido = 'S' and ptr_segnalazione = " + lIdSegnalazione + " order by c.progr desc";
		
		return s;
	}
	
	
	public String insertUtente(VfixUtenteRegistrato utente)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		// Non sono i valori in tabella ma i parametri della stored...
		String s=  "{ CALL  PACK_VFIX.VFix_Ins_Utente_Segnalatore( "
		+ "#{idUtenteRegistrato, mode=OUT ,jdbcType=DECIMAL},"
		+ "#{cognome, mode=IN, jdbcType=VARCHAR},"
		+ "#{nome, mode=IN, jdbcType=VARCHAR},"
		+ "#{email, mode=IN, jdbcType=VARCHAR},"
		+ "#{telefono, mode=IN, jdbcType=VARCHAR},"
		+ "#{telefono2, mode=IN, jdbcType=VARCHAR},"
		+ "#{username, mode=IN, jdbcType=VARCHAR},"
		+ "#{pwd, mode=IN, jdbcType=VARCHAR},"
		+ "#{indirizzo, mode=IN, jdbcType=VARCHAR},"
		+ "#{codFisc, mode=IN, jdbcType=VARCHAR},"
		+ "#{documIdent, mode=IN, jdbcType=VARCHAR},"
		+ "#{token, mode=IN, jdbcType=VARCHAR},"
		+ "#{bearer, mode=IN, jdbcType=VARCHAR})}";
	
		return s;
	}

	public String insertSegnalazione(VfixSegnalazione datiSegnalazione)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Ins_Segnalazione( "
		+ "#{idSegnalazione, mode=INOUT, jdbcType=DECIMAL},"
        + "#{dataSegnalazione, mode=IN, jdbcType=TIMESTAMP},"
        + "#{ptrUtente, mode=IN, jdbcType=DECIMAL},"
        + "#{note, mode=IN, jdbcType=VARCHAR},"
        + "#{indicazioni, mode=IN, jdbcType=VARCHAR},"
        + "#{subject, mode=IN, jdbcType=VARCHAR},"              
        + "#{fkTipologia, mode=IN, jdbcType=DECIMAL},"
        + "#{ncOk, mode=IN, jdbcType=CHAR},"
        + "#{appName, mode=IN, jdbcType=VARCHAR},"
        + "#{device, mode=IN, jdbcType=VARCHAR})}";        
		
		return s;
	}
	

	public String insertSitoSegnalazione(VfixSitoSegnalazione datiSitoSegnalazione)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Ins_Sito_Segnalazione( "
		+ "#{ptrSegnalazione, mode=IN,jdbcType=DECIMAL},"
		+ "#{latitudine, mode=IN,jdbcType=DECIMAL},"
        + "#{longitudine, mode=IN,jdbcType=DECIMAL},"
    	+ "#{indirizzoSito, mode=IN,jdbcType=VARCHAR},"
    	+ "#{codVia, mode=IN,jdbcType=DECIMAL},"
    	+ "#{codCivico, mode=IN,jdbcType=DECIMAL},"
    	+ "#{barrato, mode=IN,jdbcType=VARCHAR},"
    	+ "#{indirizzoSistema, mode=IN,jdbcType=VARCHAR},"
    	+ "#{ptrMunic, mode=IN,jdbcType=DECIMAL},"
    	+ "#{infoExt, mode=IN,jdbcType=VARCHAR})}";
		
		return s;
	}

	
	public String insertFotografia(VfixFotografie fotografia)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Ins_Fotografia( "
		+ "#{idFotografia, mode=INOUT,jdbcType=DECIMAL},"
		+ "#{ptrSegnalazione, mode=IN,jdbcType=DECIMAL},"				
		+ "#{nomeFile, mode=IN,jdbcType=VARCHAR},"
        + "#{nomeFileThumb, mode=IN,jdbcType=VARCHAR},"
    	+ "#{interno, mode=IN,jdbcType=CHAR},"
    	+ "#{ptrUtente, mode=IN,jdbcType=VARCHAR},"
    	+ "#{nomeFileThumbMid, mode=IN,jdbcType=VARCHAR})}";
		
		return s;
	}

	
	public String insertCommento(VfixCommenti datiCommento)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Ins_Commento( "
		+ "#{ptrSegnalazione,jdbcType=DECIMAL},"
		+ "#{testo,jdbcType=VARCHAR},"				
		+ "#{firma,jdbcType=VARCHAR},"
        + "#{titolo,jdbcType=VARCHAR},"
    	+ "#{interno, mode=IN,jdbcType=CHAR},"
    	+ "#{ptrUtente, mode=IN,jdbcType=VARCHAR},"
    	+ "#{idea,jdbcType=CHAR})}";
		return s;
	}
	
	
	public String selectStatistiche()
	{
		String s = null;
		s = "select 1 tipo, 'Segnalazioni ricevute' descr, trim(to_char (count(*), '999G999')) quanti from vfix_segnalazione a where a.valida in ('S', 'V') " +
		"and a.cod_comune = " + sCodiceComune + " " +
        " and a.app_name = 'IRIS' " +
        "union all " +
        "select 2 tipo, 'Segnalazioni concluse' descr, trim(to_char (count(*), '999G999')) quanti from  " +
        "(select * from vfix_iter_segnalazione a, vfix_segnalazione b, " +
        "(select ptr_segnalazione , max(data_oper) data from vfix_iter_segnalazione group by ptr_segnalazione) x " +
        "where a.ptr_segnalazione = x.ptr_segnalazione " +
        "and a.data_oper = x.data " +
        "and a.ptr_tipo_stato in (30, 70) and b.id_segnalazione = a.ptr_segnalazione and b.valida in ('S', 'V')" +
        "and b.cod_comune = " + sCodiceComune + " " +
        "and b.app_name = 'IRIS') " +
        "union all " +
        "select 3 tipo, 'Ricevute ultima settimana' descr, trim(to_char (count(*), '999G999')) quanti from vfix_segnalazione a " +
        "where trunc(a.data_segnalazione, 'DD') <= trunc(sysdate,'DD') " +
        "and trunc(a.data_segnalazione, 'DD') >= trunc(sysdate - 7,'DD') " +
        "and a.valida in ('S', 'V') " +
        "and a.cod_comune = " + sCodiceComune + " " +        
        "and a.app_name = 'IRIS' " +
        "union all " +
        "select 4 tipo, 'Ricevute ultimo mese' descr, trim(to_char (count(*), '999G999')) quanti from vfix_segnalazione a " +
        "where trunc(a.data_segnalazione,'DD') <= trunc(sysdate,'DD')  " +
        "and trunc(a.data_segnalazione,'DD') >= trunc(add_months(sysdate, -1),'DD') " +
        "and a.valida in ('S', 'V') " +
        "and a.cod_comune = " + sCodiceComune + " " +        
        "and a.app_name = 'IRIS' " +
        "union all " +        
/*        "select 4 tipo, 'Ricevute ultimo mese' descr, trim(to_char (count(*), '999G999')) quanti from  " +
        "(select * from vfix_iter_segnalazione a, vfix_segnalazione b, " +
        "(select ptr_segnalazione , max(data_oper) data from vfix_iter_segnalazione group by ptr_segnalazione) x " +
        "where a.ptr_segnalazione = x.ptr_segnalazione " +
        "and a.data_oper = x.data " +
        "and a.ptr_tipo_stato in (30,70) " +
        "and trunc(a.data_oper,'DD') <= trunc(sysdate,'DD')  " +
        "and trunc(a.data_oper,'DD') >= trunc(add_months(sysdate, -1),'DD') " +
        "and b.id_segnalazione = a.ptr_segnalazione and b.valida in ('S', 'V')" +
        "and b.cod_comune = " + sCodiceComune + " " +        
        "and b.app_name = 'IRIS') " +
        "union all " + */
        "select 5 tipo, 'Ricevute ultimo anno' descr, trim(to_char (count(*), '999G999')) quanti from vfix_segnalazione a " +
        "where trunc(a.data_segnalazione, 'DD') <= trunc(sysdate,'DD') " +
        "and trunc(a.data_segnalazione, 'DD') >= trunc(add_months(sysdate, -12),'DD') " +
        "and a.valida in ('S', 'V') " +
        "and a.cod_comune = " + sCodiceComune + " " +        
        "and a.app_name = 'IRIS' " +
        "order by tipo";
		
		return s;
	}

	public String selectRipartizione()
	{
		String s = null;
		
        s = "select b.descrizione, count(*) quanti, to_char(count(*)*100 / AA.quanti, '90D00')  as percentuale " +
        "from vfix_segnalazione a, vfix_tipo_problema b, vfix_sito_segnalazione s, " +
        "(select count(*) quanti from vfix_segnalazione a where  a.valida in ('S', 'V')) AA " +
        "where a.valida in ('S', 'V') " +
        "and a.cod_comune = " + sCodiceComune + " " +        
        "and b.id_tipo = a.fk_tipologia " +
        "and s.ptr_segnalazione = a.id_segnalazione and s.ptr_munic = s.ptr_munic " +
        "group by a.fk_tipologia, b.descrizione, AA.quanti " +
        "order by quanti desc";
		
		return s;
	}
	
	
	public String deleteCommento(VfixCommenti datiCommento)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Del_Commento( "
		+ "#{ptrSegnalazione,jdbcType=DECIMAL},"
    	+ "#{progr,jdbcType=DECIMAL})}";
		return s;
	}

	
	public String rimuoviSegnalazioneLogica(VfixInopportune datiInopportuna)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Inopportuna( "
		+ "#{ptrSegnalazione,jdbcType=DECIMAL},"
    	+ "#{motivo,jdbcType=VARCHAR})}";
		return s;
	}

	
	// legge per predisporre i dati di scrittura nel SIT
	public String selectforSIT(Long lIdSegnalazione)
	{
		
		String s = null;
		if (lIdSegnalazione != 0)
			s = "select s.ID_SEGNALAZIONE, s.SUBJECT, s.FK_TIPOLOGIA, s.valida, tp.DESCRIZIONE tipologia, decode(it.ptr_tipo_stato, 50, 20, 60, 20, it.ptr_tipo_stato) ptr_tipo_stato, " +
			"decode(it.ptr_tipo_stato, 50, 'In carico', 60, 'In carico', ts.DESCRIZIONE) stato, TO_DATE (TO_CHAR(data_segnalazione, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_segnalazione, " +
			"TO_DATE (TO_CHAR(it.data_oper, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_oper, ss.LATITUDINE, ss.LONGITUDINE, " +
			"nvl(s.INDICAZIONI, '-') indicazioni, ss.PTR_MUNIC, it.ptr_tipo_stato cod_stato_reale, ts.DESCRIZIONE stato_reale " +					
			"from vfix_segnalazione s, vfix_sito_segnalazione ss, vfix_tipo_problema tp, vfix_tipo_stato ts, " +
			"(select data_oper, ptr_tipo_stato from vfix_iter_segnalazione where id_riga in (select max(id_riga) from vfix_iter_segnalazione where ptr_segnalazione = " + lIdSegnalazione.toString() + ")) it " +
			"where ss.PTR_SEGNALAZIONE = s.ID_SEGNALAZIONE and s.valida in ('S', 'V') and tp.id_tipo = s.FK_TIPOLOGIA	and ts.ID_TIPO = it.ptr_tipo_stato and ptr_segnalazione = " + lIdSegnalazione.toString();
		else
/*			s="select * from (select s.ID_SEGNALAZIONE, s.SUBJECT, s.FK_TIPOLOGIA, s.valida, tp.DESCRIZIONE tipologia, decode(it.ptr_tipo_stato, 50, 20, 60, 20, it.ptr_tipo_stato) ptr_tipo_stato, " +
			"decode(it.ptr_tipo_stato, 50, 'In carico', 60, 'In carico', ts.DESCRIZIONE) stato, " + 
			"TO_DATE (TO_CHAR(data_segnalazione, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_segnalazione, TO_DATE (TO_CHAR(it.data_oper, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
			"ss.LATITUDINE, ss.LONGITUDINE, nvl(s.INDICAZIONI, '-') indicazioni, ss.PTR_MUNIC, it.ptr_tipo_stato cod_stato_reale, ts.DESCRIZIONE stato_reale from vfix_segnalazione s, vfix_sito_segnalazione ss, vfix_tipo_problema tp, vfix_tipo_stato ts, (select data_oper, ptr_tipo_stato, ptr_segnalazione " +
			"from vfix_iter_segnalazione where id_riga in (select max(id_riga) from vfix_iter_segnalazione group by ptr_segnalazione)) it " + 
			"where ss.PTR_SEGNALAZIONE = s.ID_SEGNALAZIONE and s.valida in ('S', 'V') and tp.id_tipo = s.FK_TIPOLOGIA and ts.ID_TIPO = it.ptr_tipo_stato and it.ptr_segnalazione = s.id_segnalazione order by s.data_segnalazione desc, s.id_segnalazione desc) where rownum <= 2000";
*/	
		s="select * from (select s.ID_SEGNALAZIONE, s.SUBJECT, s.FK_TIPOLOGIA, s.valida, tp.DESCRIZIONE tipologia, decode(it.ptr_tipo_stato, 50, 20, 60, 20, it.ptr_tipo_stato) ptr_tipo_stato, " +
		"decode(it.ptr_tipo_stato, 50, 'In carico', 60, 'In carico', ts.DESCRIZIONE) stato, " + 
		"TO_DATE (TO_CHAR(data_segnalazione, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_segnalazione, TO_DATE (TO_CHAR(it.data_oper, 'yyyy-MM-dd HH24:MI:SS'), 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
		"ss.LATITUDINE, ss.LONGITUDINE, nvl(s.INDICAZIONI, '-') indicazioni, ss.PTR_MUNIC, it.ptr_tipo_stato cod_stato_reale, ts.DESCRIZIONE stato_reale from vfix_segnalazione s, vfix_sito_segnalazione ss, vfix_tipo_problema tp, vfix_tipo_stato ts, (select data_oper, ptr_tipo_stato, ptr_segnalazione " +
		"from vfix_iter_segnalazione where id_riga in (select max(id_riga) from vfix_iter_segnalazione group by ptr_segnalazione)) it " + 
		"where ss.PTR_SEGNALAZIONE = s.ID_SEGNALAZIONE and s.valida in ('S', 'V') and tp.id_tipo = s.FK_TIPOLOGIA and ts.ID_TIPO = it.ptr_tipo_stato and it.ptr_segnalazione = s.id_segnalazione and s.id_segnalazione < 22157 order by s.data_segnalazione desc, s.id_segnalazione desc)";
			
		return s;
	}

	
	// per inserire in ITB
	/*public String inserisciITB(VfixSegnalazione datiSegn, VfixSitoSegnalazione datiSS, )
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		BEGIN
		  PACKAGE_NAME.PROCEDURE_NAME(parameter_value, ...);
		END;
		
		
		String s= "";
		return s;
	}*/


	/*********   FUNZIONI PER IL SIT **********/
	public String inserisciforSIT(VfixSegnalazione_SIT theSegn)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		/*String s = "{ CALL  PRC_IRIS_SEGNALAZIONI_INS ( "
		+ "#{idSegnalazione, mode=IN,jdbcType=DECIMAL},"				
		+ "#{subject, mode=IN,jdbcType=DECIMAL},"
		+ "#{fkTipologia, mode=IN,jdbcType=DECIMAL},"
		+ "#{tipologia, mode=IN,jdbcType=VARCHAR},"
		+ "#{ptrTipoStato, mode=IN,jdbcType=DECIMAL},"		
		+ "#{stato, mode=IN,jdbcType=VARCHAR},"	
		+ "#{dataSegnalazione, mode=IN,jdbcType=TIMESTAMP},"		
		+ "#{dataUltimaOperazione, mode=IN,jdbcType=TIMESTAMP},"
		+ "#{latitudine, mode=IN,jdbcType=DECIMAL},"
        + "#{longitudine, mode=IN,jdbcType=DECIMAL},"
		+ "#{esito, mode=OUT,jdbcType=VARCHAR})}";*/		
		
		
		CDbUtils dbu = new CDbUtils();
		String s = String.format("insert into geolayer.IRIS_SEGNALAZIONI " + 
	    "(ID_SEGNALAZIONE, OGGETTO, COD_TIPOLOGIA, TIPOLOGIA, COD_STATO, STATO, DATA_SEGNALAZIONE, DATA_ULTIMA_OPERAZIONE, SHAPE, COD_STATO_REALE, STATO_REALE, " + 
		"INDICAZIONI, PTR_MUNIC) " +
	    "values (%d, '%s', %d, '%s', %d, '%s', to_date('%s', 'dd/MM/yyyy HH24:MI:SS'), to_date('%s', 'dd/MM/yyyy HH24:MI:SS'), " +
	    "MDSYS.SDO_GEOMETRY( 2001, 4326, MDSYS.SDO_POINT_TYPE(%s, %s, NULL ), NULL, NULL ), %d, '%s', '%s', %d)", 
	    theSegn.getIdSegnalazione(), theSegn.getSubject().replaceAll("'","''"), theSegn.getFkTipologia(), theSegn.getTipologia().replaceAll("'","''"),
	    theSegn.getPtrTipoStato(), theSegn.getStato().replaceAll("'","''"), dbu.dtGetDate(theSegn.getDataSegnalazione()), dbu.dtGetDate(theSegn.getDataUltimaOperazione()),
	    theSegn.getLongitudine().toString(), theSegn.getLatitudine().toString(), 
	    theSegn.getPtrTipoStatoReale(), theSegn.getStatoReale(), theSegn.getIndicazioni().replaceAll("'","''"), theSegn.getPtrMunic());

		return s;
	}
	
	public String deleteforSIT(Long lIdSegnalazione)
	{
		String s = "delete geolayer.IRIS_SEGNALAZIONI";
		if (lIdSegnalazione != 0L) s += " where ID_SEGNALAZIONE = " + lIdSegnalazione.toString();
		return s;
	}
		
	public String updateforSIT(VfixSegnalazione_SIT theSegn)
	{
		CDbUtils dbu = new CDbUtils();
		String s = "update geolayer.IRIS_SEGNALAZIONI SET " +
				 "COD_TIPOLOGIA = " + theSegn.getFkTipologia().toString() + "," +
				 "TIPOLOGIA = '" + theSegn.getTipologia().replaceAll("'","''") + "'," +
				 "COD_STATO = '" + theSegn.getPtrTipoStato().toString() + "'," +
				 "COD_STATO_REALE = '" + theSegn.getPtrTipoStatoReale().toString() + "'," +
				 "STATO = '" + theSegn.getStato().replaceAll("'","''") + "'," +				
				 //"VALIDA = '" + theSegn.getValida() + "'," +
				 "DATA_ULTIMA_OPERAZIONE = to_date ('" + dbu.dtGetDate(theSegn.getDataUltimaOperazione()) + "', 'dd/MM/yyyy HH24:MI:SS') " +
				 //"DATA_ULTIMA_OPERAZIONE = sysdate " +
				 " where ID_SEGNALAZIONE = " + theSegn.getIdSegnalazione().toString();
		return s;
	}
	
	
	public String getInfoSegnalazione(Long lIdSegnalazione)
	{
		String s = "SELECT b.id_segnalazione, a.latitudine, a.longitudine, a.indirizzo_sito, to_char(b.data_segnalazione, 'dd/mm/yyyy') data, b.subject, b.indicazioni, " +
                 "d.descrizione tipologia, c.id_utente_registrato, c.cognome, c.nome, decode (b.nc_ok, 'S', c.cognome || ' ' || c.nome, c.cognome || ' ' || c.nome) nc, b.nc_ok, c.telefono, c.email, " +
                 "em.ptr_tipo_stato, ts.descrizione, PACK_VFIX.sdescrizionesubstato(a.ptr_segnalazione) sub_descrizione, b.fk_tipologia, nvl(b.ex_id, 0) ex_id, " +
                 "PACK_VFIX.getultassegn(" + lIdSegnalazione + ") ult_ass," +
                 " decode (i.ptr_segnalazione, null, 0, 1) inopp, b.valida, nvl(i.motivo, '') motivo, " +
                 " PACK_VFIX.listasegncorrelate(" + lIdSegnalazione + ") listasegn, a.ptr_munic, m.descrizione descrMunic, a.info_ext, TO_CHAR(nrsolocommenti.quanti) quanti, " +
                 " TO_CHAR(nrproposte.quanti) quanti_prop, b.note, is_segn_riservata(" + lIdSegnalazione + ") riservata " +
                 "FROM vfix_sito_segnalazione a, vfix_segnalazione b, vfix_utente_registrato c, vfix_tipo_problema d, vfix_municipalita m, " +
                 " (select count(*) quanti  from vfix_commenti comm where comm.idea='N' and comm.ptr_segnalazione = " + lIdSegnalazione + ") nrsolocommenti, " +
                 " (select count(*) quanti  from vfix_commenti comm where comm.idea='S' and comm.ptr_segnalazione = " + lIdSegnalazione + ") nrproposte, " +
                 "(select a.ptr_tipo_stato from vfix_iter_segnalazione a where " +
                 " a.data_oper = (select max(x.data_oper) from vfix_iter_segnalazione x " +
                 "where x.ptr_segnalazione = " + lIdSegnalazione + ") " +
                 "and a.ptr_segnalazione = " + lIdSegnalazione + ") em, vfix_tipo_stato ts, vfix_inopportune i " +
                 "WHERE b.id_segnalazione = a.ptr_segnalazione and c.id_utente_registrato = b.ptr_utente  and d.id_tipo = b.fk_tipologia " +
                 "and ts.id_tipo = em.ptr_tipo_stato " +
                 "and a.ptr_segnalazione = " + lIdSegnalazione + " " +
                 "and b.app_name = 'IRIS' " +
                 "and i.ptr_segnalazione (+)= a.ptr_segnalazione " +
                 "and m.cod_munic = a.ptr_munic";
         return s;
	}
	
	public String getEmailDestinatari(String theListaDestinatari, int idMunic)
	{
		String s = "SELECT a.id_referente, d.email FROM vfix_referente a, vfix_ref_munic d where a.id_referente in (" + theListaDestinatari + ") and a.app_name = 'IRIS'" +
                   " and d.ptr_ref = a.id_referente and d.ptr_munic = " + idMunic;
         return s;
	}
	

	public String getListaReferenti_Ext(int theIdPrb, int theMunic, String sInfoExt)
	{
		sInfoExt = sInfoExt.replace("'", "''");
		
		String s = "select d.email, d.descrizione, a.cp from vfix_prb_ref a, vfix_referente b, " +
	            " vfix_tipo_problema c,  vfix_ref_munic d " +
	            " where b.id_referente = a.ptr_ref and c.id_tipo = a.ptr_prb and a.ptr_prb = " + theIdPrb +
	            " and b.app_name = 'IRIS' and a.ptr_munic = " + theMunic +
	            " and d.ptr_ref = a.ptr_ref  and d.ptr_munic = a.ptr_munic " +
	            " and a.ptr_ref_ext ='" + sInfoExt + "'";
         return s;
	}
	
	
	public String getOpenData ()
	{
		String s = "SELECT to_char(a.id_segnalazione) as id_segnalazione, to_char(a.data_segnalazione, 'DD/MM/YYYY') data, nvl(a.subject, '-') as oggetto, " +
        " nvl(tp.descrizione, '-') as tipo, nvl(s.indirizzo_sistema, '-') indirizzo_sistema, nvl(to_char(s.latitudine), '-') as latitudine, " + 
		 "nvl(to_char(to_char(s.longitudine)), '-') as longitudine, " +
        " m.descrizione municipalita, " +
        " PACK_VFIX.echostatomascherato(ts.id_tipo,ts.descrizione) stato " +
        " FROM vfix_segnalazione a, vfix_sito_segnalazione s, vfix_tipo_problema tp, vfix_municipalita m, " +
        " vfix_tipo_stato ts, " +
        " (select  a.ptr_segnalazione, a.ptr_tipo_stato from vfix_vista_stati a, " +
        "     (select ptr_segnalazione, max(data_oper) data from vfix_vista_stati a group by ptr_segnalazione) AA " +
        "     where a.ptr_segnalazione = AA.ptr_segnalazione and a.data_oper = AA.data) VV " +
        "  where s.ptr_segnalazione = a.id_segnalazione " +
        "  and a.valida in ('S', 'V') " +
        "  and tp.id_tipo = a.fk_tipologia " +
        "  and m.cod_munic = s.ptr_munic " +
        "  and VV.ptr_segnalazione = a.id_segnalazione " +
        "  and ts.id_tipo = VV.ptr_tipo_stato " +
        " order by a.id_segnalazione desc";
		
		return s;
	}
	
	public String selectIterSegnalazioneMasked(Long lIdSegnalazione)
	{
		// ci sarebbe la casistica masked e non masked (ma come fare per l'utente?)...
		
		String s = null;
		
		s= "SELECT 1 ord,a.ptr_segnalazione, a.ptr_tipo_stato, TO_CHAR(a.data_oper, 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
	        "decode (a.ptr_tipo_stato, 10, '-', a.note) note, b.descrizione || PACK_VFIX.sdescrizionesubstato(a.ptr_segnalazione, a.ptr_tipo_stato, ':') descrizione,  " +
	        "nvl(decode (a.ptr_tipo_stato, 10, PACK_VFIX.listareferentinew(a.ptr_segnalazione, a.ptr_tipo_stato, 'N'), PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,f.id_referente)), '-') refs,  " +
	        "decode (a.ptr_tipo_stato, 20, '-', PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,c.id_referente))  des_oper, c.id_referente, e.ptr_riga  " +
	        "FROM vfix_iter_segnalazione a, vfix_tipo_stato b, vfix_referente c, vfix_utenti d, vfix_assegnazione e, vfix_referente f " +
	        "where b.id_tipo = a.ptr_tipo_stato and a.ptr_segnalazione = " + lIdSegnalazione +
	        "and d.id_utente = a.ptr_utente and c.id_referente = d.ptr_refer " +
	        "and f.id_referente (+)= e.ptr_ref and e.ptr_riga (+)= a.id_riga  " +
	        "and a.ptr_tipo_stato in (10) " +
	        "union all " +
	        "SELECT 3 ord,a.ptr_segnalazione, a.ptr_tipo_stato, TO_CHAR(a.data_oper, 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
	        "a.note, b.descrizione || PACK_VFIX.sdescrizionesubstato(a.ptr_segnalazione, a.ptr_tipo_stato, ':') descrizione,  " +
	        "nvl(decode (a.ptr_tipo_stato, 10, PACK_VFIX.listareferentinew(a.ptr_segnalazione, a.ptr_tipo_stato, 'N'), PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,f.id_referente)), '-') refs,  " +
	        "decode (a.ptr_tipo_stato, 20, '-', PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,c.id_referente))  des_oper, c.id_referente, e.ptr_riga  " +
	        "FROM vfix_iter_segnalazione a, vfix_tipo_stato b, vfix_referente c, vfix_utenti d, vfix_assegnazione e, vfix_referente f " +
	        "where b.id_tipo = a.ptr_tipo_stato and a.ptr_segnalazione = " + lIdSegnalazione +
	        "and d.id_utente = a.ptr_utente and c.id_referente = d.ptr_refer " +
	        "and f.id_referente (+)= e.ptr_ref and e.ptr_riga (+)= a.id_riga  " +
	        "and a.ptr_tipo_stato in (30,70) " +
	        "union all " +
	        "SELECT 2 ord,a.ptr_segnalazione, decode (a.ptr_tipo_stato, 20, a.ptr_tipo_stato, 20) ptr_tipo_stato, TO_CHAR(a.data_oper, 'yyyy-MM-dd HH24:MI:SS') data_oper, " +
	        "decode (a.ptr_tipo_stato, 50, '-', 60, '-', a.note) note, decode(a.ptr_tipo_stato, 40, b.descrizione, 'In carico')  || PACK_VFIX.sdescrizionesubstato(a.ptr_segnalazione, a.ptr_tipo_stato, ':') descrizione,  " +
	        "nvl(decode (a.ptr_tipo_stato, 40, '-', PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,f.id_referente)), '-') refs,  " +
	        "decode (a.ptr_tipo_stato, 40, PACK_VFIX.DesRefSpecializzatoByIdSegn(a.ptr_segnalazione,c.id_referente), '-')  des_oper, c.id_referente, e.ptr_riga  " +
	        "FROM vfix_iter_segnalazione a, vfix_tipo_stato b, vfix_referente c, vfix_utenti d, vfix_assegnazione e, vfix_referente f, " +
	        "(SELECT max (l.id_riga) massimo " +
	        "  FROM vfix_iter_segnalazione l " +
	        "  where l.ptr_segnalazione = " + lIdSegnalazione +
	        "  and l.ptr_tipo_stato  in (20, 50, 60, 40)) x " +
	        "where b.id_tipo = a.ptr_tipo_stato and a.ptr_segnalazione = " + lIdSegnalazione +
	        "and d.id_utente = a.ptr_utente and c.id_referente = d.ptr_refer " +
	        "and f.id_referente (+)= e.ptr_ref and e.ptr_riga (+)= x.massimo " +
	        "and a.id_riga = x.massimo " +
	        "order by ord desc";
		
		return s;
	}
	
	
	// cancella la segnalazione (solo nel caso di un rollback... fatto perché con myBatis non ho ancora capito come funziona)
	public String cancellaSegnalazioneCompleta(VfixSegnalazione datiSegnalazione)
	{
		// OCCHIO CHE E' POSIZIONALE!!!
		String s= "{ CALL  PACK_VFIX.VFix_Del_Segnalazione_Completa( "
		+ "#{idSegnalazione, mode=INOUT, jdbcType=DECIMAL})}";
		return s;
	}
	
	public String getEmailSegnalatore (Long theSegn)
	{
		String s = "select a.email from vfix_utente_registrato a, vfix_segnalazione b " +
                "where b.id_segnalazione = " + theSegn + " and a.id_utente_registrato = b.ptr_utente";
         return s;
	}
	
	
	public String bControllaAbilitUtente (Long nSegn, String sStringaUtente, String sMunicList)
	{
		String s = "SELECT b.ptr_refer FROM vfix_referente a, vfix_utenti b, vfix_assegnazione c, " +
                "vfix_sito_segnalazione s " +
                "where b.id_utente = '" + sStringaUtente + "' and a.id_referente = b.ptr_refer " +
                "and c.ptr_ref = a.id_referente and c.valida = 'S' and c.ptr_segnalazione = " + nSegn +
                " and s.ptr_segnalazione = c.ptr_segnalazione " +
                " and s.ptr_munic in (" + sMunicList + ") ";
        return s;
	}
	
	
	public String sLeggiListaMunic (String sStringaUtente)
	{
		String s = "SELECT pack_vfix.municperutente('" + sStringaUtente + "') lista_mun from dual";
        return s;
	}
	
	
	// funzioni di aggiornamento di una segnalazione
	public String updateStatoSegnalazione(VfixIterSegnalazione datiIter)
	{
		String s= "{ CALL  PACK_VFIX.VFix_Upd_Stato_Segnalazione( "
		+ "#{ptrSegnalazione, mode=IN, jdbcType=DECIMAL},"
		+ "#{ptrTipoStato, mode=IN, jdbcType=DECIMAL},"
		+ "#{note, mode=IN, jdbcType=VARCHAR},"
		+ "#{ptrUtente, mode=IN, jdbcType=DECIMAL},"
		+ "#{idRiga, mode=INOUT, jdbcType=DECIMAL})}";
		return s;
	}

	public String updateStatoAssegnazione(VfixAssegnazione datiAssegnazione)
	{
		String s= "{ CALL  PACK_VFIX.VFix_Upd_Assegnazione( "
		+ "#{ptrSegnalazione, mode=IN, jdbcType=DECIMAL},"
		+ "#{ptrRef, mode=IN, jdbcType=DECIMAL},"
		+ "#{ptrRiga, mode=IN, jdbcType=DECIMAL})}";
		return s;
	}
	
	public String insertInfoConclusa(VfixConcluse datiConclusa)
	{
		String s= "{ CALL  PACK_VFIX.VFix_Ins_Conclusa( "
		+ "#{ptrSegnalazione, mode=IN, jdbcType=DECIMAL},"
		+ "#{ptrSubtipo, mode=IN, jdbcType=DECIMAL})}";
		return s;
	}

	public String updateLogPrgEsterno(VfixLogMimuv datiPrgEsterno)
	{
		String s= "{CALL  PACK_VFIX.VFix_Upd_Log_Mimuv( "
		+ "#{ptrSegnalazione, mode=IN, jdbcType=DECIMAL},"
		+ "#{valido, mode=IN, jdbcType=CHAR})}";
		return s;
	}

	public String getUltimoAssegnatario (Long theSegn)
	{
		String s = "select nvl(PACK_VFIX.getultassegn(" + theSegn + "), -1) ultass FROM dual";
        return s;
	}

	
	public String getUrlNotifica (Short nReferente)
	{
		String s = "select nvl(a.url_notifiche, '') url_notifiche FROM vfix_referente a where a.id_referente = " + nReferente;
        return s;
	}

	public String getDescrTipologiaProblema (Integer nTipo)
	{
		String s = "SELECT a.descrizione FROM vfix_tipo_problema a where a.id_tipo = " + nTipo;
        return s;
	}
	
	
	public String nearestPoint (String Lat, String Lon)
	{
		String s = "SELECT CIVICO_NUM as civico,  CIVICO_SUB as lettera,  CIVICO_VIA as codice_via,  SUBCOD_VIA, " +          
       "DENOMINAZIONE_X_VIA as completo,  DENOMINAZIONE_X_SESTIERE,scom_cod FROM geodbgt.civico_vista r, geodbgt.ascom_vista m WHERE " +
       "SDO_NN(r.shape,  MDSYS.SDO_GEOMETRY( 2001, 4326, MDSYS.SDO_POINT_TYPE("+ Lon +", " + Lat + ", NULL ), NULL, NULL ),'sdo_num_res=1') = 'TRUE' " +
       "and SDO_CONTAINS(m.shape,MDSYS.SDO_GEOMETRY( 2001, 4326, MDSYS.SDO_POINT_TYPE("+ Lon +", " + Lat + ", NULL ), NULL, NULL )) = 'TRUE' " +
       "and m.a_scom_ty ='05' "; 
        return s;
	}
	
	
	public String sLeggiListaMunicipalita (String sStringaUtente)
	{
		String s = "SELECT pack_vfix.municperutente('" + sStringaUtente + "') lista_mun from dual";
        return s;
	}
	
	
	public String sControllaAbilitUtente (Long nSegn, String sStringaUtente, String sMunicList)
	{
		String s = "SELECT b.ptr_refer FROM vfix_referente a, vfix_utenti b, vfix_assegnazione c, " +
		           "vfix_sito_segnalazione s " +
		           "where b.id_utente = '" + sStringaUtente + "' and a.id_referente = b.ptr_refer " +
		           "and c.ptr_ref = a.id_referente and c.valida = 'S' and c.ptr_segnalazione = " + nSegn +
		           " and s.ptr_segnalazione = c.ptr_segnalazione " +
		           " and s.ptr_munic in (" + sMunicList + ") " +
		           " and b.front_iris2_on = 'S'";
        return s;
	}

	
	public String getInfoSegnalatore(Long lIdSegnalazione)
	{
		String s = "select ur.id_utente_registrato, ur.email, ur.token "
				+ "from vfix_utente_registrato ur, vfix_segnalazione s "
				+ "where ur.id_utente_registrato = s.ptr_utente "
				+ "and s.id_segnalazione = " + lIdSegnalazione;
        return s;
	}
	
	
	public String bPrbTrattatoDaMunic (Short idTipo, Short idMunic)
	{
		String s = "SELECT count(*) quanti FROM vfix_prb_ref a where a.ptr_munic = " + idMunic + " and a.ptr_prb = " + idTipo;
        return s;
	}

	public String desRefSpecializzatoByIdSegn (Long nSegn, Short nReferente)
	{
		String s = "SELECT PACK_VFIX.DesRefSpecializzatoByIdSegn (" + nSegn + "," + nReferente + ") as txt_assegnatario from dual";
        return s;
	}

	public String lastuserByIdSegn (Long nSegn)
	{
		String s = "select id_utente from (SELECT u.id_utente FROM vfix_assegnazione ass, vfix_utenti u, vfix_utente_munic um, vfix_sito_segnalazione ss "
        + "WHERE ass.valida = 'S' and u.PTR_REFER = ass.PTR_REF and um.PTR_MUNIC = ss.PTR_MUNIC and ss.ptr_segnalazione = ass.ptr_segnalazione "
        + "and ass.ptr_segnalazione = " + nSegn + " and um.ptr_utente = u.id_utente and u.valido_al is null) where rownum = 1";
        return s;
	}
	
	public String getInfoForUpdExternal (Long nSegn)
	{
		String s = "select pack_vfix.DesRefSpecializzatoByIdSegn (" + nSegn + ", PACK_VFIX.GETULTASSEGN (" + nSegn + ")) as ufficio, "
				+ "decode (ts.id_tipo, 40, 20, 50, 20, 60, 20, 70, 20, ts.id_tipo) id_tipo, ts.descrizione stato, "
				+ "PACK_VFIX.sdescrizionesubstato(" + nSegn + ")  as motivo, em.note, TO_CHAR(em.data_oper,'dd/MM/yyyy HH24:MI:SS') as data_oper "
				+ "from vfix_segnalazione s, vfix_tipo_stato ts, "
				+ "(select * from vfix_iter_segnalazione a where a.data_oper = "
				+ "(select max(x.data_oper) from vfix_iter_segnalazione x  where x.ptr_segnalazione = " + nSegn + ") "
				+ "and a.ptr_segnalazione = " + nSegn + ") em "
				+ "where id_segnalazione = " + nSegn + " "
				+ "and ts.id_tipo = em.ptr_tipo_stato ";
        return s;
	}
		
	public String getListaSegnalazioniDaAggiornare (Long nSegn)
	{
		String s = selectListaSegnalazioni(0L);
		s += " where A.id_segnalazione in (select id_iris from vfix_tmp_mimuv t, vfix_sito_segnalazione ss  where t.tema = 'AGGIORNA' and t.trattato = 'N' and ss.ptr_segnalazione = t.id_iris)"; 
        return s;
	}

	
	
/*
 *  Per il controllo delle abilitazioni utente
 *  
           
           ---- quindi...
              Private Function bLivelloOk() As Boolean
        Dim wsDBFIX As New ServiceVFix_DB.Service_VFix
        Dim ds As Data.DataSet
        If Session("livello_Abil") = N_LIVELLO_AMMINISTRATIVO AndAlso Not CType(ConfigurationManager.AppSettings("limitaAdmin"), Boolean) Then Return True
        If Session("livello_Abil") = N_LIVELLO_READONLY Then
            CType(Me.Page.FindControl("labLimiteAbil"), Label).Text = "ATTENZIONE: puoi soltanto consultare questa segnalazione perché hai privilegi di sola visura."
            Return False
        End If
        Try
            ds = wsDBFIX.bControllaAbilitUtente(Context.User.Identity.Name, CType(Request.QueryString("id"), Integer))
            If ds.Tables(0).Rows.Count = 0 Then Return False
            ' altrimenti è potenzialmente sua ma serve un altro...
	
	*/
}
