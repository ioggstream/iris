package it.venis.iris2.oggettidbiris;

public class VfixCommentiKey {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Long ptrSegnalazione;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_COMMENTI.PROGR
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Short progr;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.PTR_SEGNALAZIONE
	 * @return  the value of VFIX_COMMENTI.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Long getPtrSegnalazione() {
		return ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.PTR_SEGNALAZIONE
	 * @param ptrSegnalazione  the value for VFIX_COMMENTI.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrSegnalazione(Long ptrSegnalazione) {
		this.ptrSegnalazione = ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_COMMENTI.PROGR
	 * @return  the value of VFIX_COMMENTI.PROGR
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Short getProgr() {
		return progr;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_COMMENTI.PROGR
	 * @param progr  the value for VFIX_COMMENTI.PROGR
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setProgr(Short progr) {
		this.progr = progr;
	}
}