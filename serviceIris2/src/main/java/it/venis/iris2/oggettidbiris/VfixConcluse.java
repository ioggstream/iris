package it.venis.iris2.oggettidbiris;

public class VfixConcluse {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_CONCLUSE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Long ptrSegnalazione;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column VFIX_CONCLUSE.PTR_SUBTIPO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	private Short ptrSubtipo;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_CONCLUSE.PTR_SEGNALAZIONE
	 * @return  the value of VFIX_CONCLUSE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Long getPtrSegnalazione() {
		return ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_CONCLUSE.PTR_SEGNALAZIONE
	 * @param ptrSegnalazione  the value for VFIX_CONCLUSE.PTR_SEGNALAZIONE
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrSegnalazione(Long ptrSegnalazione) {
		this.ptrSegnalazione = ptrSegnalazione;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column VFIX_CONCLUSE.PTR_SUBTIPO
	 * @return  the value of VFIX_CONCLUSE.PTR_SUBTIPO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public Short getPtrSubtipo() {
		return ptrSubtipo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column VFIX_CONCLUSE.PTR_SUBTIPO
	 * @param ptrSubtipo  the value for VFIX_CONCLUSE.PTR_SUBTIPO
	 * @mbggenerated  Thu Jun 20 11:01:45 CEST 2019
	 */
	public void setPtrSubtipo(Short ptrSubtipo) {
		this.ptrSubtipo = ptrSubtipo;
	}
}