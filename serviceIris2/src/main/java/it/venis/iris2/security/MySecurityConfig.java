package it.venis.iris2.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;


public class MySecurityConfig {

	public MySecurityConfig() {
		// TODO Auto-generated constructor stub
	}
	
	@Configuration	
	public static  class WebSecurityConfigNew extends WebSecurityConfigurerAdapter {
		  @Override
		  protected void configure(HttpSecurity http) throws Exception {
		    http.csrf().disable().authorizeRequests()
		        .antMatchers("/").permitAll()       
		        .antMatchers(HttpMethod.GET, "/login").permitAll()
		        .antMatchers(HttpMethod.POST, "/login").permitAll()
		        .antMatchers(HttpMethod.OPTIONS, "/login").permitAll()

		        .antMatchers("/search").permitAll()
		        .antMatchers(HttpMethod.GET, "/leggitipistato").permitAll()
		        .antMatchers(HttpMethod.GET, "/leggitipiproblema").permitAll()
		        .antMatchers(HttpMethod.GET, "/leggitipisubstato").permitAll()                
		        .antMatchers(HttpMethod.GET, "/leggimunicipalita").permitAll()        
		        .antMatchers(HttpMethod.GET, "/leggilistasegnalazioni").permitAll()        
		        .antMatchers(HttpMethod.GET, "/leggistatistiche").permitAll()
		        .antMatchers(HttpMethod.GET, "/leggiripartizione").permitAll()
		        .antMatchers(HttpMethod.GET, "/leggitipioperazioni").permitAll()
		        .antMatchers(HttpMethod.GET, "/getopendata").permitAll()
		        
		        .antMatchers(HttpMethod.GET, "/nearestpoint").permitAll()

		        .antMatchers(HttpMethod.POST, "/operazione").authenticated()
		        .antMatchers(HttpMethod.GET, "/operazione").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/operazione").permitAll()
		        
		        /*.antMatchers(HttpMethod.POST, "/postoperazione").authenticated()
		        .antMatchers(HttpMethod.GET, "/postoperazione").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/postoperazione").permitAll()*/
		        
		        .antMatchers(HttpMethod.POST, "/inserisci").authenticated()
		        .antMatchers(HttpMethod.GET, "/inserisci").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/inserisci").permitAll()

		        .antMatchers(HttpMethod.POST, "/commenta").authenticated()
		        .antMatchers(HttpMethod.GET, "/commenta").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/commenta").permitAll()
		        
		        .antMatchers(HttpMethod.POST, "/rimuovicommento").authenticated()
		        .antMatchers(HttpMethod.GET, "/rimuovicommento").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/rimuovicommento").permitAll()

		        .antMatchers(HttpMethod.POST, "/rimuovisegnalazione").authenticated()
		        .antMatchers(HttpMethod.GET, "/rimuovisegnalazione").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/rimuovisegnalazione").permitAll()
		        
		        .antMatchers(HttpMethod.POST, "/setnewendpoint").permitAll()
		        .antMatchers(HttpMethod.GET, "/setnewendpoint").permitAll()
		        .antMatchers(HttpMethod.OPTIONS, "/setnewendpoint").permitAll()
		        
		        .antMatchers(HttpMethod.POST, "/disableendpoint").permitAll()
		        .antMatchers(HttpMethod.GET, "/disableendpoint").permitAll()
		        .antMatchers(HttpMethod.OPTIONS, "/disableendpoint").permitAll()
		        
		        .antMatchers(HttpMethod.POST, "/echouser").authenticated()
		        .antMatchers(HttpMethod.GET, "/echouser").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/echouser").permitAll()
		        
		        .antMatchers(HttpMethod.POST,"/searchmine").authenticated()
		        .antMatchers(HttpMethod.GET,"/searchmine").authenticated()
		        .antMatchers(HttpMethod.OPTIONS, "/searchmine").permitAll()
		        
				.antMatchers(HttpMethod.POST, "/abilitato").authenticated()
				.antMatchers(HttpMethod.GET, "/abilitato").authenticated()
				.antMatchers(HttpMethod.OPTIONS, "/abilitato").permitAll()


				// TODO - da risolvere -- UTILITY per ricreare layer ed elasticsearch
				.antMatchers(HttpMethod.GET,"/inserisciitb").permitAll()
		        .antMatchers(HttpMethod.GET, "/componilistasegnalazioni").permitAll()

				.antMatchers(HttpMethod.GET, "/prelogintoken").permitAll()
				.antMatchers(HttpMethod.POST, "/prelogintoken").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/prelogintoken").permitAll()
				
				.antMatchers(HttpMethod.GET, "/checkmysegn").permitAll()
				.antMatchers(HttpMethod.POST, "/checkmysegn").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/checkmysegn").permitAll()
				
				.antMatchers(HttpMethod.GET, "/inserisciext").denyAll()
				.antMatchers(HttpMethod.POST, "/inserisciext").authenticated()
					
		        .anyRequest().authenticated()
		        .and()
		        // We filter the api/login requests       
		        .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
		        // And filter other requests to check the presence of JWT in header
		        .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		  }

		  
		  @Override
		  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		    // Create a default account
		    auth.inMemoryAuthentication()
		        .withUser("UTENTE")
		        .password("PASSWORD")
		        .roles("ADMIN");
		  }
	}
	
	
	@Order(1)
	@Configuration
	public static class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
		  @Override
		  protected void configure(HttpSecurity http) throws Exception {
			  http.csrf().disable()
		      .antMatcher("/api/**")
		      .httpBasic()
		      .and()
		      .authorizeRequests()
		      .antMatchers(HttpMethod.GET,"/api/aggiornasegnalazione").authenticated()
		      .antMatchers(HttpMethod.POST,"/api/aggiornasegnalazione").authenticated()
   		      .antMatchers(HttpMethod.GET,"/api/bonificaMimuv").authenticated()
		      .antMatchers(HttpMethod.POST,"/api/bonificaMimuv").authenticated()
		      
   		      .antMatchers(HttpMethod.GET,"/api/bonificaMimuv").denyAll()
		      .antMatchers(HttpMethod.POST,"/api/aggiornamentoGenerico").authenticated()
		      
		      .antMatchers(HttpMethod.GET,"/api/coopaggiornasegnalazione").authenticated()
		      .antMatchers(HttpMethod.POST,"/api/coopaggiornasegnalazione").authenticated()
		      .antMatchers(HttpMethod.GET,"/api/coopallineasegnalazione").authenticated()
		      .antMatchers(HttpMethod.POST,"/api/coopallineasegnalazione").authenticated()
		      
		      
		      .antMatchers(HttpMethod.GET, "/api/inserisciext").denyAll()
		      .antMatchers(HttpMethod.POST, "/api/inserisciext").authenticated()
		      
		      
   			  .antMatchers(HttpMethod.GET, "/api/updsf").authenticated()
   			  .antMatchers(HttpMethod.POST, "/api/updsf").authenticated()

   			  .antMatchers(HttpMethod.POST, "/api/getInfoSegnalatore").authenticated()
   			  .antMatchers(HttpMethod.GET, "/api/getInfoSegnalatore").authenticated()
   			  .antMatchers(HttpMethod.GET, "/api/exportsegn").authenticated()
   			  .antMatchers(HttpMethod.POST, "/api/exportsegn").authenticated()
   			  
   			  .antMatchers(HttpMethod.POST, "/api/rimuovisegnalazione").authenticated()
		      .antMatchers(HttpMethod.GET, "/api/rimuovisegnalazione").authenticated()
		      .antMatchers(HttpMethod.OPTIONS, "/api/rimuovisegnalazione").permitAll()

   			  .antMatchers(HttpMethod.POST, "/api/elasticsearchupdate").authenticated()
		      .antMatchers(HttpMethod.GET, "/api/listaSegnalazioniIris").denyAll()		  
		      .antMatchers(HttpMethod.POST, "/api/listaSegnalazioniIris").authenticated();
		  }
		  
		  @Override
		  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		    // Create a default account
		    auth.inMemoryAuthentication()
		        .withUser("UTENTE")
		        .password("PASSWORD")
		        .roles("ADMIN");
		}
	}
	
}

