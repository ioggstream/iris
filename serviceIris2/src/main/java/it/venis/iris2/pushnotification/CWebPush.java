package it.venis.iris2.pushnotification;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ExecutionException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.lang.JoseException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;

public class CWebPush {
	private String keyPublic;
	private String keyPrivate;
	private String keySubject;
	
	public CWebPush() {
		java.util.ResourceBundle mybundle=java.util.ResourceBundle.getBundle("application");
		keyPrivate = mybundle.getString("pkey.private");
		keyPublic = mybundle.getString("pkey.public");
		keySubject = mybundle.getString("pkey.subject");
	}

	public boolean doIt (String endpoint, String payload)
	{
		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
		    Security.addProvider(new BouncyCastleProvider());
		JsonParser parser = new JsonParser();
		JsonObject jsonObj = (JsonObject) parser.parse(endpoint);
		String theEndpoint = jsonObj.get("endpoint").getAsString();
		String theP256dh = jsonObj.get("keys").getAsJsonObject().get("p256dh").getAsString();
		String theAuth = jsonObj.get("keys").getAsJsonObject().get("auth").getAsString();	
		Subscription sub = new Subscription();
		
		sub.endpoint = theEndpoint;
		sub.keys = sub.new Keys(theP256dh, theAuth);
		
		try {
			this.sendPushMessage (sub, payload.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	private void sendPushMessage(Subscription sub, byte[] payload) {

		  // Figure out if we should use GCM for this notification somehow
		  Notification notification;
		  PushService pushService;

		    try {
				// Create a notification with the endpoint, userPublicKey from the subscription and a custom payload
				notification = new Notification(sub, new String (payload));
				// Instantiate the push service, no need to use an API key for Push API
				pushService = new PushService (keyPublic, keyPrivate, keySubject);
				// Send the notification
				pushService.send(notification);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchProviderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JoseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
