//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.10.09 alle 12:56:42 PM CEST 
//


package it.venis.iris2.irisinterface;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.venis.iris2.irisinterface package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.venis.iris2.irisinterface
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DatiIris }
     * 
     */
    public DatiIris createDatiIris() {
        return new DatiIris();
    }

    /**
     * Create an instance of {@link RispostaCooperatore }
     * 
     */
    public RispostaCooperatore createRispostaCooperatore() {
        return new RispostaCooperatore();
    }

    /**
     * Create an instance of {@link RispostaIris }
     * 
     */
    public RispostaIris createRispostaIris() {
        return new RispostaIris();
    }

    /**
     * Create an instance of {@link DatiIris.Utente }
     * 
     */
    public DatiIris.Utente createDatiIrisUtente() {
        return new DatiIris.Utente();
    }

    /**
     * Create an instance of {@link DatiIris.Foto }
     * 
     */
    public DatiIris.Foto createDatiIrisFoto() {
        return new DatiIris.Foto();
    }

    /**
     * Create an instance of {@link DatiCooperatore }
     * 
     */
    public DatiCooperatore createDatiCooperatore() {
        return new DatiCooperatore();
    }

}
