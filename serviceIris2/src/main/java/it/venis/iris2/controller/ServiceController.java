package it.venis.iris2.controller;

import it.venis.iris2.AppConfigDbIris;
import it.venis.iris2.db.utils.CDbUtils;
import it.venis.iris2.elastic.utils.CElasticSUtils;
import it.venis.iris2.oggettidbiris.VfixEndpointNotifiche;
import it.venis.iris2.oggettidbiris.dao.CustomMapper;
import it.venis.iris2.pushnotification.CWebPush;
import it.venis.iris2.security.CGestioneUtente;
import it.venis.iris2.utils.CUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@RestController
public class ServiceController {

    private static final String template = "Hello, %s!";
    
	CElasticSUtils esU = new CElasticSUtils();

    static org.slf4j.Logger cat = org.slf4j.LoggerFactory.getLogger(ServiceController.class);
    
    private PlatformTransactionManager transactionManager;
    public void setTransactionManager( PlatformTransactionManager transactionManager)
    {    
      this.transactionManager = transactionManager;  
    }

    
	// Elenco delle segnalazioni
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/leggilistasegnalazioni",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggilistasegnalazioni(HttpServletRequest request,
			  					   @RequestParam(value="servizio", defaultValue="") String servizio)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaSegnalazioni(0L);

		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
		
    	ctx.close();
    	
        return sJson;     
    }
	
	
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggitipiproblema",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggitipiproblema(HttpServletRequest request,
			  					   @RequestParam(value="servizio", defaultValue="") String servizio,
			  					   @RequestParam(value="dimeOn", defaultValue="N") String dimeOn)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaProblemi(dimeOn);
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }

	
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggitipistato",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggitipistato(HttpServletRequest request,
			  					   @RequestParam(value="servizio", defaultValue="") String servizio)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaTipoStato();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }

	
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggitipioperazioni",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggitipioperazioni(HttpServletRequest request)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaTipoOperazioni();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }
	
	

	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggitipisubstato",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggitipisubstato(HttpServletRequest request,
			  					   @RequestParam(value="servizio", defaultValue="") String servizio)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaSubStato();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }
	
	
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggimunicipalita",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggimunicipalita(HttpServletRequest request,
			  					   @RequestParam(value="servizio", defaultValue="") String servizio)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectListaMunicipalita();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }
	
	
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/componilistasegnalazioni",  produces={"application/json"},  method = RequestMethod.GET)
    public String componilistasegnalazioni(@RequestParam(value="idsegnalazione", defaultValue="") String sIdSegnalazione)
    {	
		CDbUtils dbu = new CDbUtils();
		return dbu.componilistasegnalazioni(sIdSegnalazione, false);
    }
	
	// collegamento con ElasticSearch..
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/doES",  produces={"application/json"},  method = RequestMethod.GET)
    public String doES(HttpServletRequest request)
    {	
		//Settings settings = Settings.settingsBuilder().put("cluster.name", "elasticsearch").build();
	    TransportClient client2 = null;
		try {
			client2 = new PreBuiltTransportClient(Settings.EMPTY).addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// on shutdown
		client2.close();	
		System.out.println ("Finito");
        return null;     
    }
	
	
	@CrossOrigin
	@GetMapping
    @RequestMapping(value = "/search",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String cerca(HttpServletRequest request,
    							   @RequestParam(value="id_segnalazione", defaultValue="0") Long lIdSegnalazione,
			  					   @RequestParam(value="q", defaultValue="") String sQuery,
			  					   @RequestParam(value="from", defaultValue="0") int wFrom,
			  					   @RequestParam(value="size", defaultValue="10") int wSize,
			  					   @RequestParam(value="sort", defaultValue="") String sSort,
			  					   @RequestParam(value="filtro", defaultValue="") String sFiltro,
			  					   @RequestParam(value="dtinizio", defaultValue="") String dtInizio,
			  					   @RequestParam(value="dtfine", defaultValue="")  String dtFine)
    {	
		String sJson = esU.bCerca(lIdSegnalazione, sQuery, wFrom, wSize, sSort, sFiltro, dtInizio, dtFine, null);
		
        return sJson;     
    }

	@CrossOrigin
	@GetMapping
    @RequestMapping(value = "/searchmine",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String cercaMie(HttpServletRequest request,
    							   @RequestParam(value="id_segnalazione", defaultValue="0") Long lIdSegnalazione,
			  					   @RequestParam(value="q", defaultValue="") String sQuery,
			  					   @RequestParam(value="from", defaultValue="0") int wFrom,
			  					   @RequestParam(value="size", defaultValue="10") int wSize,
			  					   @RequestParam(value="sort", defaultValue="") String sSort,
			  					   @RequestParam(value="filtro", defaultValue="") String sFiltro,
			  					   @RequestParam(value="dtinizio", defaultValue="") String dtInizio,
			  					   @RequestParam(value="dtfine", defaultValue="")  String dtFine)
    {	
		String sIdUtente = new CGestioneUtente().getCurrentUserIrisID();
		if (sIdUtente == null) return "{}";
		String sJson = esU.bCerca(lIdSegnalazione, sQuery, wFrom, wSize, sSort, sFiltro, dtInizio, dtFine, sIdUtente);
        return sJson;     
    }
	
	

	// --------------------------------------- INSERIMENTO DI UNA NUOVA SEGNALAZIONE
	// Operazione che potrà essere effettuata solo se l'utente è accreditato nel sistema (es. Spid)
		
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/inserisci",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String inserisci(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiSegnalazione", defaultValue="") String datiSegnalazione)
    {	
		CDbUtils dbu = new CDbUtils();
    	if (datiSegnalazione.equals("") && body != null) datiSegnalazione = body.toString();
		return dbu.InserisciDatisuDB(datiSegnalazione, request.getHeader("Authorization"), false);     
    }
	 

	// --------------------------------------- INSERIMENTO DI UN COMMENTO 
	
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/commenta",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String commenta(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiCommento", defaultValue="") String datiCommento)
    {	
		
		CDbUtils dbu = new CDbUtils();
		if (datiCommento.equals("") && body != null) datiCommento = body.toString();
		//datiCommento = "{\"datiCommento\":{\"PTR_SEGNALAZIONE\":50483, \"INTERNO\":N, \"TESTO\":\"aaa\", \"TITOLO\":\"aaa\"}}";
		return dbu.InserisciDatiCommento(datiCommento);     
    }
	

	// --------------------------------------- ELIMINAZIONE DI UN COMMENTO 
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/rimuovicommento",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String rimuovicommento(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiCommento", defaultValue="") String datiCommento)
    {	
		
		//datiCommento = "{\"PTR_SEGNALAZIONE\":39054,\"PROGR\":2}";
		
		CDbUtils dbu = new CDbUtils();
		if (datiCommento.equals("") && body != null) datiCommento = body.toString();
		return dbu.EliminaCommento(datiCommento);     
    }
	

	// --------------------------------------- ELIMINAZIONE DI UNA SEGNALAZIONE 
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/rimuovisegnalazione",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String rimuovisegnalazione(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiInopportuna", defaultValue="") String datiInopportuna)
    {	
		//datiInopportuna = "{\"PTR_SEGNALAZIONE\":39054,\"MOTIVO\":\"Estremamente maleducata!\"}";
		CDbUtils dbu = new CDbUtils();
		if (datiInopportuna.equals("") && body != null) datiInopportuna = body.toString();		
		return dbu.EliminaSegnalazioneLogica(datiInopportuna);     
    }
	
	
	
	// --------------------------------------- LANCIO DI UNA NOTIFICA
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/notifica",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean notifica(@RequestParam(value="endpoint", defaultValue="") String endpoint,
    							   		   @RequestParam(value="payload", defaultValue="") String payload)
    {
		CWebPush wp = new CWebPush();
		try {
			if (wp.doIt(endpoint, payload))	return true;
			else return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
			return false;
		}
    }
	
	// --------------------------------------- SALVA ENDPOINT
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/setnewendpoint",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean setnewendpoint(HttpServletRequest request, @RequestParam(value="endpoint", defaultValue="") String endpoint, 
    							  @RequestParam(value="useragent", defaultValue="") String useragent, 
    							  @RequestBody (required = false) String body)
    {
		System.out.println (endpoint);
		
		CDbUtils dbu = new CDbUtils();
		if (endpoint.equals("") && useragent.equals("") && body != null)
		{
			JsonObject jsonObj = (JsonObject) new JsonParser().parse(body);
			endpoint = jsonObj.get("endpoint").getAsString();
			useragent = jsonObj.get("useragent").getAsString();
		}
		
		
		if (dbu.oSettaUtente(request.getHeader("Authorization"), null) == null) return false;  // crea subito l'utente...
		if (dbu.inserisciNuovoEndpoint(endpoint, useragent))
			return true;
		return false;
    }
	

	// --------------------------------------- SALVA ENDPOINT
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/disableendpoint",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean disableendpoint(@RequestParam(value="endpoint", defaultValue="") String endpoint, 
    							  @RequestBody (required = false) String body)
    {
		System.out.println (endpoint);
		
		CDbUtils dbu = new CDbUtils();
		if (endpoint.equals("") && body != null)
		{
			JsonObject jsonObj = (JsonObject) new JsonParser().parse(body);
			endpoint = jsonObj.get("endpoint").getAsString();
		}
		return dbu.disabilitaEndpoint(endpoint);
    }

	
	// --------------------------------------- LANCIO NOTIFICA BROADCAST (non utilizzato)
	
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/notificabc",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public boolean notificabc(@RequestParam(value="payload", defaultValue="") String payload)
    {
		if (payload.isEmpty()) payload = "{\"body\":\"Test invio da Iris 2\"}";
		
		CWebPush wp = new CWebPush();		
		CDbUtils dbu = new CDbUtils();
		
		try {
			List<VfixEndpointNotifiche> le = dbu.selectEndpoint(null);  // poi potrò indicare lo user
			
			for (VfixEndpointNotifiche theEp : le) {			
				wp.doIt(theEp.getEndpoint(), payload);		
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
    }

	
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggistatistiche",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggistatistiche(HttpServletRequest request)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectStatistiche();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }


	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/leggiripartizione",  produces={"application/json"},  method = RequestMethod.GET)
    public String leggiripartizione(HttpServletRequest request)
    {	
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		String sJson = "";
		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.selectRipartizione();
	
		sJson = new  GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(lista);
	
    	ctx.close();
    	
        return sJson;     
    }

	
	@CrossOrigin (origins = "*" )
	@PostMapping	
	  @RequestMapping(value = "/login",  method={RequestMethod.GET, RequestMethod.POST})
	  public String myLogin ()
	  {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (!(authentication instanceof AnonymousAuthenticationToken)) {
			    String currentUserName = authentication.getName();
			    return currentUserName;
			}
			
			return "no";
	  }
	
	@CrossOrigin (origins = "*" )
	@PostMapping	
	  @RequestMapping(value = "/inserisciitb",  method={RequestMethod.GET, RequestMethod.POST})
	  public String inserisciitb ()
	  {
			CDbUtils dbu = new CDbUtils();
			return dbu.componilistamarker();
	  }
	
	
	// legge gli open data
	@CrossOrigin (origins = "*" )
	@GetMapping
    @RequestMapping(value = "/getopendata",  produces={"application/csv"},  method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getopendata(HttpServletRequest request)
    {	
		ResponseEntity respEntity = null;
		HttpHeaders responseHeaders = new HttpHeaders();
		
    	AnnotationConfigApplicationContext ctx;
    	
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfigDbIris.class);
		ctx.refresh();		

		CustomMapper cm = ctx.getBean(CustomMapper.class); 
		
		List<Map<String,String>> lista = cm.getOpenData();
    	ctx.close();
    	
    	StringBuilder sRigheBuilder = new StringBuilder();

    	sRigheBuilder.append("\"ID_SEGNALAZIONE\";\"DATA\";\"OGGETTO\";\"TIPO\";\"LUOGO\";\"LATITUDINE\";\"LONGITUDINE\";\"MUNICIPALITA\";\"STATO\"");
    	int idxNodi = 0;
    		try {
  	    	for (idxNodi = 0; idxNodi < lista.size(); idxNodi++)
  	    	{
				sRigheBuilder.append ("\n");    	
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("ID_SEGNALAZIONE").toString() + "\"");
				sRigheBuilder.append (";");
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("DATA").toString() + "\"");
				sRigheBuilder.append (";");    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("OGGETTO").toString().replace(";", ",").replace("\"", " ") + "\"");
				sRigheBuilder.append (";");    		    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("TIPO").toString().replace(";", ",").replace("\"", " ") + "\"");
				sRigheBuilder.append (";");    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("INDIRIZZO_SISTEMA").toString().replace(";", ",").replace("\"", " ") + "\"");
				sRigheBuilder.append (";");    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("LATITUDINE").toString().replace("\"", " ") + "\"");
				sRigheBuilder.append (";");
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("LONGITUDINE").toString().replace("\"", " ") + "\"");
				sRigheBuilder.append (";");    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("MUNICIPALITA").toString().replace("\"", " ") + "\"");
				sRigheBuilder.append (";");    		
				sRigheBuilder.append ("\"" + lista.get(idxNodi).get("STATO").toString().replace("\"", " ") + "\"");
  	}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    	
    	byte[] byteArray = null;
		try {
			byteArray = sRigheBuilder.toString().getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
		if (byteArray != null)
		{
		    try {
		    	// iso8859-1 
		    	responseHeaders.add("content-disposition", "filename='SegnalazioniIRIS.csv'");
		    	respEntity = new ResponseEntity(byteArray, responseHeaders, HttpStatus.OK);
		    	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				respEntity = new ResponseEntity ("File Not Found", HttpStatus.OK);
			}			
		}
   	
	    return respEntity;
    }
	
	// funzione di aggiornamento iter / cambio stato / notifiche relative
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/operazione",  produces={"application/json"},  method={RequestMethod.GET, RequestMethod.POST})
    public String operazione(HttpServletRequest request, @RequestBody (required = false) String body, 
    		@RequestParam(value="datiOperazione", defaultValue="") String datiOperazione)
    {	
		CDbUtils dbu = new CDbUtils();
    	if (datiOperazione.equals("") && body != null) datiOperazione = body.toString();		
		return dbu.sAggiornaPostOperazione(datiOperazione);
    }
	
	
	@CrossOrigin (origins = "*" )
    @RequestMapping(value = "/nearestpoint",  produces={"application/json"},  method = RequestMethod.GET)
    public String nearestpoint(HttpServletRequest request,
			  			 	  @RequestParam(value="lat", defaultValue="0") String lat,
			  			 	  @RequestParam(value="lon", defaultValue="0") String lon)
    {	
 
		String sJson = "";
		CDbUtils dbu = new CDbUtils();

		sJson = dbu.getNearestPoint(lat, lon);
        return sJson;     
    }

	
	// --------------------------------------- INSERIMENTO DI UNA NUOVA SEGNALAZIONE
	// Operazione che potrà essere effettuata solo se l'utente è accreditato nel sistema (es. Spid)
		
	@CrossOrigin (origins = "*" )
	@PostMapping
    @RequestMapping(value = "/inserisciext",  produces={"application/json"},  method={RequestMethod.POST})
    public String inserisciExt(HttpServletRequest request, @RequestBody (required = false) String body,
    							   @RequestParam(value="datiSegnalazione", defaultValue="") String datiSegnalazione)
    {	
		CDbUtils dbu = new CDbUtils();
    	if (datiSegnalazione.equals("") && body != null) datiSegnalazione = body.toString();
		return dbu.InserisciDatisuDB(datiSegnalazione, null, true);     
    }
	
}
