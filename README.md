# README #

Il presente documento descrive le operazioni da seguire per installare Iris.

### IRIS 2 ###

* Iris è il sistema per consentire ai cittadini di segnalare problemi di manutenzione urbana nel territorio e contribuire così alla loro soluzione
* 2.0

## Immagini e GIF
![Screenshot](iris_frontend/public/screenshot.png)

# Indice
-  [Come iniziare](#come-iniziare)

-  [Come contribuire](#come-contribuire)

-  [Licenza](#licenza)


# Come iniziare  
## Dipendenze
 - NodeJS
 - Workbox (`npm i --g workbox-cli`)
 - Backend Spring Boot capace di risolvere le richieste del frontend. Necessita di un database Oracle
 - Elastic Search

## Database configuration
Per configurare il database è sufficiente generarlo mediante il file \database\
Successivamente, a database creato, eseguire le istruzioni sql contenute \database\configure_tables.sql in maniera tale da inserire i dati di configurazione.
La tabella VFIX_TIPO_STATO permette di configurare il colore con il quale il frontend presenta i diversi stati di una segnalazione.

## Come installare
    git clone https://Comune_Venezia@bitbucket.org/Comune_Venezia/iris.git
	
	Per la parte iris_frontend:
    npm i
    npm start8
	
	

# Come contribuire

## Struttura delle directory / branch per FRONT END
 - public: *Cartella che contiene i file statici del progetto*
 - src: *Cartella che contiene i file necessari all'applicazione che verranno compilati in fase di build*
 - .env: *Variabili di ambiente del progetto*
 - .env.production: *Variabili di ambiente usate soltanto in produzione*
 - server.js: *Server Node da eseguire in produzione*
 - workbox-config.js: *Impostazioni relative a Workbox*
  
## Struttura delle directory / branch per BACK END (\serviceIris2)
 - src: *Cartella che contiene i file necessari all'applicazione che devono essere compilati. Si consiglia l'utilizzo di Eclipse STS. Il progetto è Spring Boot*

    
## Community

### Segnalazione bug e richieste di aiuto
Venis S.p.A. - Unità Operativa Servizi e Sistemi (riuso@venis.it)



# Licenza
La licenza per questo repository è la GNU AGPL v3.0 or later.


## Autori e Copyright
Venis S.p.A.
  

## Licenze software dei componenti di terze parti
Tutti i componenti utilizzati sono rilasciati con licenza open source